# TOC
1. Get Started
2. Directory and File Structure
    1. WordPress
    2. Static Assets
        1. CSS
        2. JavaScript 
        3. Images
    3. WP-Content / Themes, Plugins, Uploads, etc.
    4. Config Directory
    5. \_old_site
3. Config Files 
    1. Gulp
    2. Webpack
    3. WP
    4. Edits
        1. `gulp/config.js`
        2. `webpack/config.*.js`
        3. `wp/config.db.php`
4. Git
    1. Branching
    2. Deploying
5. Gulp/Webpack
    1. Gulp Tasks
        1. The `default` task
        2. The `prod` task
    2. Webpack
        1. Local Environment
        2. Staging/Production Environment
6. I'm Sure I've Forgotten Something.

# 1. Get Started <small>`tl;dr`</small>

1. Clone or download this repo to your local computer.
1. Install WordPress to the `wp` directory.
3. Run `yarn` or `npm install` from the site's root directory.
4. Install `gulp-cli` globally if not already installed. `npm i -g gulp-cli`
5. Make changes to the site's config files ( See *3. Config Files §D. Edits* below ).
6. Run `gulp` in the site's root directory to spin it all up.

> Note: gulp 4.0 introduced breaking changes from the old gulp global module, requiring gulp-cli to be installed.


# 2. Directory and File Structure

We run a pretty custom file structure for our wordpress sites, but it's not terribly difficult to navigate once you know what's what.

## I. WordPress <small>`site_dir/wp/`</small>
We move all of the WordPress files to their own directory, except for the index.php and wp-config.php files. These two files stay in the root. This means that in order to log in, you must point your browser to `example.com/wp/wp-admin` instead of the traditional `example.com/wp-admin`.

You'll note that the actual WP files are under `gitignore.` This is to help save some time with initial pull times from repo to repo. This does add a step to each migration however, as wordpress needs to be installed and moved to the correct directory. We recommend running the following from the site's root directory to download and move WordPress using only one line in the command line:

If you have `curl` installed:

`$ curl -O https://wordpress.org/latest.tar.gz && tar -xzf latest.tar.gz -C wp --strip-components=1`

If you have `wget` installed:

`$ wget https://wordpress.org/latest.tar.gz && tar -xzf latest.tar.gz -C wp --strip-components=1`

## II. Static Assets  <small>`site_dir/src/`</small>
All precompiled static assets (CSS, JavaScript, images) are found in `site_dir/src`, according to their respective directories (e.g., site_dir/src/css). Currently, the site is set up to use gulp.js to build these assets and place them into the `site_dir/dist` directory, which is where they are served from on the site.

### A. CSS
We write all of our css in SCSS, which is then compiled using gulp.

- Entrypoint: `site_dir/src/css/app.scss`
- File Served:
	- Local and Staging: `site_dir/dist/app.css`
	- Production: `site/dist/app.min.css`

### B. JavaScript 
We write all of our JavaScript using vanilla ES6 syntax. This is compiled by webpack into ES5. On your local machine, webpack is piped through gulp, but you'll need to run an npm script to build production files (see *gulp/webpack* section below).

- Entrypoint: `site_dir/src/javascript/app.js`
- File Served:
	- Local and Staging: `site_dir/dist/app.js`
	- Production: `site/dist/app.min.js`
-  Modules: We typically split our JS codebase up into directories and files, and then include those files using ES6 import/export statements.

### C. Images
Content images will most likely be handled through WordPress's media library, but the images directory will hold all theme-based images and svgs. These images and svgs, found in `site_dir/src/images` will be optimized via gulp.

- Entrypoint: `site_dir/src/images/**/*`
- Files served: `site_dir/dist/images/**/*`

It's recommended, but not necessary, to place SVGs in `site_dir/src/images/icons/` and `site_dir/dist/images/icons`. There is a custom function `get_file()` in the WordPress functions directory that is set up to look for SVGs in this `dist` path.

## III. WP-Content / Themes, Plugins, Uploads, etc. <small>`site_dir/content/`</small>
We've moved all of the directories typically found in `wp-content` to another custom directory, `site_dir/content`. We've also already set the `wp-config.php` file to look for this directory rather than the default. The structure of this directory is exactly the same as what would be found in `wp-content` as well.

The theme for this site will be located at `site_dir/content/themes/missionbio`.

## IV. Config Directory <small>`site_dir/config/`</small>
See *3. Config Files* section below.

## V. \_old_site
This directory contains all the files for the current production version of the site. We _shouldn't_ overwrite the current site, given the suggested git branches (See *4. Git §A. Branching* section below), but I wanted to keep these files just in case.


# 3. Config Files

The files in `site_dir/config` serve as configurations for the site/build system as a whole. Inside you'll find the following:

- gulp
- webpack
- wp

## A. Gulp
The gulp directory keeps the main `config.js` file for gulp, which contains a configuration object used within each of the gulp tasks. This directory also contains each of the individual gulp tasks used by by the `default` and `prod` gulp tasks.

- `images.js` &mdash; The image task, optmizes images saved in `site_dir/src/images` and places optimized files in `site_dir/dist/images`.
- `sass.js` &mdash; Contains two tasks. `sass:dev`: The default sass/style task which compiles scss to css; and `sass:prod` which compiles *and* minifies the files for production. 
- `server.js` &mdash; The server/browser-sync task which serves the local site and uses live reload and style injection, runs webpack for javascript compilation locally.

## B. Webpack
Contains the configuration objects for webpack to transpile ES6 to browser ready ES5 javascript. Configs include:

- `config.common.js`
- `config.dev.js`
- `config.prod.js`

## C. WP
This directory holds the database configuration which is then included in `wp-config.php`. It contains the file `config.db.sample.php`. This file will need to be copied and renamed to `config.db.php` so that it can be found by `wp-config.php`. This preserves the sample file, while allowing `config.db.php` to be kept under gitignore so that usernames and passwords are not committed to the repository and made public.

## D. Edits
In order to get the site running, the following edits need to be made.

### i. `gulp/config.js`
This file is a javascript object, which contains config options for each of the gulp tasks.

• _server_

Browser-sync, which allows live reloading and style injection, works via proxying from localhost to another url. Currently, the server key is set with the following option key value pairs:

```
host: 'localhost',
port: 3000,
target: 'missionbio.local'
```

Target is the proxy address used by browser-sync, and host/port are self-explanatory. If you need to use a different port or proxy target, change these values.

>Note: This file can be committed with any changes made, but that forces other users of the repository to pull the changes in. Best practice, if they will be changed, is to set an `assume-unchanged` flag via `git` before making any changes to the server section.

>Note: Because browser-sync is currently set-up to use a proxy, you may need to add the proxy url to your `/etc/hosts` file, or create the proxy as an alias using any \*AMP software stack you might be using for local development.

• _sass_, _postCSS_, _images_

These values shouldn't need to be changed, *unless* you are modifying the site directory structure and placing static assets in different directories than the default.

> Note: If the file structure *is* changed, these values *do* need to be committed to the repository, so that other users can have the changes reflected in their local environment as well.

### ii. `webpack/config.*.js`

These files shouldn't need to be changed, *unless* the directory structure is changed with regards to where static assets are kept. If this is changed, see the previous note.

### iii. `wp/config.db.php`

As already noted, `wp-config.php` is set to look for this file in order to connect to the mysql database. For security reasons, this file is under gitignore, and a blank `config.db.sample.php` file exists in it's place. In order to create the correct file, we recommend running the following command:

> Note: The following assumes you are in the site's root directory. Adjust paths based on your current working directory.

`$ cp config/wp/config.db.sample.php config/wp/config.db.php`

Then edit your values accordingly.


# 4. Git

## I. Branching
We don't use any particular git branching strategy (e.g. git flow, etc); rather, we use two main branches tied to our two environments.

- main &mdash; The main branch is linked to our production environment, and is the current live site.
- dev &mdash; The dev branch is linked to our local and staging environments. 

The `dev` branch is set-up in beanstalk to _automatically deploy_ to the staging environment. Any change committed *will* automatically appear on the staging site.

The `main` branch is set-up in beanstalk to _manually deploy_ to the production environment. Changes *will not* automatically push, for obvious reasons.

Once a site has been initially deployed to production, we then begin to utilize *feature* branches. For example, if we are working on a new hero banner for an about page, we might create a new branch called `about-hero`. Once this work is complete locally, we then merge the new branch into `dev` for testing on the staging server, and then merge `dev` into `master`. Once merged and deployed to `master`, the feature branch is considered dead and can be removed.

## II. Deploying
When the site is ready to deploy to production, we merge `dev` into `main`, then deploy.


# 5. Gulp/Webpack

As previously mentioned, we use a combination of gulp.js and webpack for our build tools. In this section, I'll list the two main gulp tasks and mention their component tasks, as well as talk about how to run webpack for your local environment and then for the staging/production environements.

## I. Gulp Tasks
There are two main gulp tasks, `default` and `prod`, which are each composed of other component tasks.

### A. The `default` task
The default gulp task is ran by simply running

`$ gulp`

in the site's root directory (where `gulpfile.babel.js` is located).

The default task runs the following component tasks:
- `sass:dev` (compiles SCSS into CSS, unminified with sourcemaps)
- `images` (image optimization/minification)
- `server` (launches a local proxy server for the site at localhost:3000, with live reload and style injection)

This is the task you want to run as your work on the site locally.

### B. The `prod` task
This isn't a full "production task" (see *webpack* section below), but just an alias for the `sass:prod` task.

`sass:prod` does the same task as `sass:dev`, but also has the extra step of minifying the CSS files and removing sourcemaps.

## II. Webpack
Webpack is the tool we use to transpile ES6 into browser-ready ES5 JavaScript. THere is a big "gotcha" you should be aware of when running the local site and when prepping files for the staging and production sites.

### A. Local Environment
Webpack is piped to the `server` gulp task automatically when the `default` gulp task is ran locally. One thing to be mindful of is that webpack _does not_ produce an actual file when running `gulp` locally; rather, it streams changes to the JS files to the `browser-sync` dev server started while running `gulp`. These changes are live reloaded into the browser if you are using localhost:3000. All of this is done via memory, so no file is produced.

### B. Staging/Production Environment
For our two remote environments, a file must be produced of course. In order to do so, we must run the following commands from the site root directory (where `package.json` is located):

__For Staging__ 

`$ npm run build:staging`

This is an alias script, which actually runs the following:

`$ webpack --config ./config/webpack/config.dev.js`

This script will create `site_dir/dist/javascript/app.js`, and the other javascript files necessary to the site. They will be sourcemapped and unminified.

__For Production__

`$ npm run build:production`

which aliases to:

`gulp sass:prod && webpack --config ./config/webpack/config.prod.js`

This script will produce a minified `site_dir/dist/javascript/app.min.js`, minified versions of any other scripts necessary to run on the site, *AND* it will also produce the minified CSS files necessary for production by also running the `sass:prod` gulp task.

> Note: If these npm scripts error out, try adding the node_modules path to webpack in the scripts under `package.json`. It may be looking for a globally installed webpack node module, rather than using the one listed under dev depenedencies.

# 6. I'm Sure I've Forgotten Something.

This became way more of a novel than I originally intended. I'm sure I've forgotten something, or in my rush to complete, have not been as clear as I should. Please reach out to me for any clarification or needed resources.
