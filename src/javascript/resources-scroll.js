const ResourcesScroll = function() {

	//Resource page links
	const stickyLinks  = document.getElementsByClassName('sticky-links');

	//Other links that should have the scroll

	let pageClasses = document.getElementsByTagName('body')[0];
	let isResourcePage = pageClasses.classList.contains('page-template-page-resources');
	let isHomepage = pageClasses.classList.contains('frontpage');

	function moveToSection( scrollDuration, offsetTop ) {

		var difference = offsetTop - window.scrollY
		var scrollStep = difference / (scrollDuration / 15);
		var isPositive = ( (offsetTop - window.scrollY ) > 0 ) ? true : false;

		// Can probably refactor this...
		if( isPositive ) {
			var scrollInterval = setInterval( function() {
		        if( difference > 0 ) {
		            window.scrollBy( 0, scrollStep );
					difference = difference - scrollStep;
		        }
		        else clearInterval(scrollInterval);
    			}, 15);
		} else {
			var scrollInterval = setInterval( function() {
		        if( difference < 0 ) {
		            window.scrollBy( 0, scrollStep );
					difference = difference - scrollStep;
		        }
		        else clearInterval(scrollInterval);
    			}, 15);
		}

	}


	const addEventListenerToList = function(nodeList, event, fn) {
	    for (var i = 0, len = nodeList.length; i < len; i++) {
	        nodeList[i].addEventListener(event, fn, false);
	    }
	}

	addEventListenerToList( stickyLinks, 'click', function(e) {
		// TODO: use the cos here https://stackoverflow.com/questions/21474678/scrolltop-animation-without-jquery
		//
		e.preventDefault();
		const isStickyLink = e.target.classList.contains('sticky-links');
		if( isStickyLink ) {
			const target = e.target.dataset.hash.slice( 1 );
			const targetTop = document.getElementById(target).getBoundingClientRect();
			const offsetLeft = targetTop.left + 0;
			const offsetTop = targetTop.top + window.scrollY - 130;

			moveToSection(500, offsetTop);
		}
	});


	jQuery(window).load( function() {
		if( location.hash ) {
			const target = location.hash.slice( 1 );
			const targetTop = document.getElementById(target).getBoundingClientRect();
			const offsetLeft = targetTop.left + 0;
			const offsetTop = targetTop.top + window.scrollY - 200;

			moveToSection(500, offsetTop);
		}
	});




}

export default ResourcesScroll;
