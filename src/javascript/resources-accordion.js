const ResourcesAccordion = function() {

	let isResourcePage = document.getElementsByTagName('body')[0];
	    isResourcePage = isResourcePage.classList.contains('page-template-page-resources');


	const accordionItems = document.getElementsByClassName('accordion-title');

	Object.keys( accordionItems ).forEach(function (i) {
		accordionItems[i].addEventListener( 'click', function( e ) {
			this.nextElementSibling.classList.toggle('hidden');
			this.querySelector('.accordion-icon.plus').classList.toggle('hidden');
			this.querySelector('.accordion-icon.minus').classList.toggle('hidden');
		});
	});




}

export default ResourcesAccordion;
