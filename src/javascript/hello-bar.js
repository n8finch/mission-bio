
const HelloBar = function() {

	const globalACFSettings = global_scripts;
	const showHelloBar = sessionStorage.hideHelloBar;
	const innerWidth = window.innerWidth;
	let divPrimary = document.getElementsByClassName('tm-primary')[0];
	let stickyFullWidthNav = document.getElementById('full-width-sticky-menu');

	if( !showHelloBar ) {
		const body = document.getElementsByTagName('body')[0];
		const header = document.getElementsByTagName('header')[0];
		let helloBar = document.createElement('div');
		let topMarginBefore = '150px';
		let topMarginAfter = '100px';

		// console.log(typeof Object.keys(body.classList) );
		if( ( body.classList.contains('home') ||  body.classList.contains('transparent-header-nav') ) || 550 > innerWidth ) {
			// mobile does not need the extra margin
			( 550 < innerWidth ) ? topMarginBefore = '50px' : topMarginBefore = '0';
			topMarginAfter = '0';
		}

		//TODO: Can choose to iterate over the innerHTML to do more than one message
		helloBar.innerHTML = `<div>${globalACFSettings[0]['text']}</div><div><span id="close-hello-bar" class="ios-close-empty"><img src="/dist/images/ios-close-empty.svg" alt="close the hello bar" /></span></div>`;
		helloBar.setAttribute( 'id', 'mbio-hello-bar');
		helloBar.style.left = 0 ;
		helloBar.style.position = 'relative' ;

		header.insertBefore(helloBar, header.firstChild);

		divPrimary.style.marginTop = topMarginBefore;

		if( stickyFullWidthNav ) {
			stickyFullWidthNav.style.top = '100px';
		}

		const closeHelloBar = helloBar.querySelector('#close-hello-bar');

		closeHelloBar.addEventListener("click", function(){
			helloBar.style.display = 'none' ;
			divPrimary.style.marginTop = topMarginAfter;

			if( stickyFullWidthNav ) {
				stickyFullWidthNav.style.top = '50px';
			}


			sessionStorage.setItem( 'hideHelloBar', true );
		});
	}

}

function ready(fn) {
  if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
    console.log('doc is ready');
  } else {
    // document.addEventListener('DOMContentLoaded', doHelloBar() );
  }
}


export default HelloBar;
