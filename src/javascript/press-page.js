
// Set up the variables for Isotope
const filtersElem = document.querySelector('.press-tabs');
const listTabs = document.getElementsByClassName('press-tab');

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

/**
 * [addPostsToList description]
 * @param {[type]} returnedPosts [description]
 */
function addPostsToList( returnedPosts, isCustomEndpoint ) {

	// Get the container and clear the current posts
	const container = document.getElementsByClassName('press-items')[0];
	var counter = 1

	container.innerHTML = '';
	container.style.height = 'auto';

	//Iterate over returnedPosts
	Object.keys(returnedPosts).forEach(function (i) {

		// console.log(returnedPosts);
		// return;

		let pressItem = document.createElement('a');
		let itemLink,
			itemType,
			itemLogo,
			itemTitle;

		if( isCustomEndpoint ) {
			itemLink = returnedPosts[i].guid;
			itemType = returnedPosts[i].post_type;
			itemLogo = svgIcons[itemType];
			itemTitle = returnedPosts[i].post_title;
		} else {
			itemLink = returnedPosts[i].link;
			itemType = returnedPosts[i].type;
			itemLogo = svgIcons[itemType];
			itemTitle = returnedPosts[i].title.rendered;
		}

		const html =
			`<a href="${itemLink}">
				<div class="press-item item-${counter} ${itemType} animated fadeIn">
					<span class="item-icon">${itemLogo}</span>
					<div>
						<span class="item-type">${itemType.capitalize()}</span>
						<span class="item-title">${itemTitle}</span>
					</div>					
					<span class="item-read-more">Read More</span>
				</div>
			</a>`;

		container.insertAdjacentHTML('beforeend', html );
		counter++;
	});

}

/**
 * [getSomePosts description]
 * @param  {[type]} postType the post type from the data-filter
 */
function getSomePosts( postType ) {

	let customPostType   = '',
		isCustomEndpoint = false;

	if( '*' !== postType ) {
		postType.split('').shift();
		customPostType = postType.split('');
		customPostType.shift();
		customPostType = customPostType.join('');
	} else {
		customPostType   = 'viewallpressitems';
		isCustomEndpoint = true;
	}

	//Do the XMLHttpRequest
	var request = new XMLHttpRequest();

	request.open('GET', '/wp-json/wp/v2/' + customPostType , true);

	request.onload = function() {
	  if (request.status >= 200 && request.status < 400) {

		// Success!
	    var returnedPosts = JSON.parse(request.responseText);

		addPostsToList( returnedPosts, isCustomEndpoint );

	  } else {
	    // We reached our target server, but it returned an error
		console.warn('Hey did not get any posts.');
	  }
	};

	request.onerror = function() {
		console.error('ERROR: Hey did not get any posts.');
	};

	request.send();

}


/**
 * Run the Isotope filter
 * @param  {[type]} filtersElem [description]
 */
if( filtersElem ) {

	// const iso = new Isotope( '.press-items', {
	//   itemSelector: '.press-item',
	//   layoutMode: 'vertical'
	// });

	filtersElem.addEventListener( 'click', function( event ) {

		getSomePosts( event.target.getAttribute('data-filter') );

		// // only work with li
	    // if ( !matchesSelector( event.target, 'li' ) ) {
	    //   return;
	    // }
		//
		// var filterValue = event.target.getAttribute('data-filter');
		//
		// // use matching filter function
		// iso.arrange({ filter: filterValue });
		//
		// Object.keys(listTabs).forEach(function (i) {
		// 	   listTabs[i].classList.remove('is-checked');
		// });
		//
	    // event.target.classList.add('is-checked');
	});
}
