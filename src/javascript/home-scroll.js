
const HomeScroll = function() {

	//set some vars
	let isHomepage = document.getElementsByTagName('body')[0];
	    isHomepage = isHomepage.classList.contains('home');
	let isTransNav = document.getElementsByTagName('body')[0];
	    isTransNav = isTransNav.classList.contains('transparent-header-nav');
	let menuCTAColor = 'rgba(238, 61, 77, 1)';
	let menuCTABacgroundColor = 'rgba(255, 255, 255, 1)';
	let menuCTABacgroundColorHover = 'rgba(238, 61, 71, 1)';
	let menuCTATransiation = 'background 0.5s ease,  color 0.5 ease';

	/**
	 * Do all the homepage magic on scroll
	 * @param  {[object]} 		object    [description]
	 * @param  {[DOMelement]}	header    [description]
	 * @param  {[DOMelement]} 	logo      [description]
	 * @param  {[nodeList]} 	menuLinks [description]
	 * @param  {[DOMelement]} 	menuCTA   [description]
	 */
	const doHomepageNavScrollMagic = function( object, header, logo, menuLinks, menuCTA ) {

		// if there's any scrolling on a non-mobile phone
	 	if( object.scrollY > 0 && object.innerWidth > 550 ) {

			let alpha 			= object.scrollY * .005 + .3;
			let whitergba 		= `rgba(255, 255, 255, ${alpha})`;
			let blackrgba 		= `rgba(73, 84, 106, ${alpha})`;
			let bluergba 		= `rgba(67, 196, 227, ${alpha})`;
			let headerBoxShadow = `0 3px 3px rgba(187, 187, 187, ${alpha})`;
			let brightness 		= 0 + alpha;
			let invert 			= 1 - alpha;

			header.style.backgroundColor = whitergba;
			header.style.color = blackrgba;
			header.style.boxShadow = headerBoxShadow;

			if( object.scrollY > 100 ) {
				logo.style.filter = 'none';
			} else {
				logo.style.filter = `brightness(${brightness}) invert(${invert})`
			}

			Object.keys( menuLinks ).forEach(function (i) {
				menuLinks[i].style.color = blackrgba;
			});

			// menuCTA.style.color = menuCTAColor = whitergba;
			// menuCTA.style.backgroundColor = menuCTABacgroundColor = bluergba;
			// menuCTA.style.transition = menuCTATransiation;


		}

		// if they scroll back up to the top
		if( object.scrollY == 0 && object.innerWidth > 550 ) {

			header.style.backgroundColor = 'transparent';
			header.style.boxShadow = 'none';
			logo.style.filter = 'brightness(0) invert(1)';

			Object.keys( menuLinks ).forEach(function (i) {
				menuLinks[i].style.color = 'white';
			});

			menuCTA.style.color = menuCTAColor = 'rgba(238, 61, 77, 1)';
			menuCTA.style.backgroundColor = menuCTABacgroundColor = 'white';
		}
	}



	/**
	 * Do the shrink and grow animation on scroll
	 * @param  {[DOMelement]} header [description]
	 */
	const doNavShrinkAndGrowOnScroll = function( header ) {

		// On Scroll:
		// nav ul li.margin-bottom to 0
		// m-header div.uk-container.uk-container-center > nav padding-top: 10px
		// div.tm-site-branding min-height: 50px

		let headerLogoContainer = header.querySelector('div.tm-site-branding');
		let headerLogo 			= header.querySelector('.header-site-logo img');
		let headerNav			= header.querySelector('div.uk-container.uk-container-center > nav');
		let headerNavLinks 		= header.querySelectorAll('nav > ul > li');

		//don't do anything if the scroll is further down the page.
		// if( window.scrollY > 100 ) {
		// 	return;
		// }

		// handle all the resizing
		if( 1 < window.scrollY < 50 ) {
			headerLogo.style.maxWidth = (100 - window.scrollY) + 'px';
			headerLogo.style.minWidth = (100 - window.scrollY) + 'px';
			headerLogo.style.width = (100 - window.scrollY) + 'px';


			let navPadding = 45 - window.scrollY;
			if( window.scrollY > 10 && navPadding > 9 ) {
				headerNav.style.paddingTop = navPadding + 'px';
			}

			let logoContainerPadding = 100 - window.scrollY;
			if( logoContainerPadding > 49 ) {
				headerLogoContainer.style.minHeight = logoContainerPadding + 'px';
			}

			let navLinksBottom = 20 - window.scrollY;
			if( navLinksBottom > 0 ) {
				Object.keys( headerNavLinks ).forEach(function (i) {
					headerNavLinks[i].style.marginBottom = navLinksBottom + 'px';
				});
			}
		}


		//switch out logos and set width

		if( 64 > window.scrollY ) {
			headerLogo.setAttribute( 'src', '/dist/images/missionbio-logo-stacked.svg' );
		}
		if( 64 <= window.scrollY ) {
			headerLogo.setAttribute( 'src', '/dist/images/missionbio-logo-horizontal.svg' );
		}
		if( 65 < window.scrollY ) {
			headerLogo.style.maxWidth = '200px';
			headerLogo.style.minWidth = '200px';
			headerLogo.style.width = '200px';
			headerLogoContainer.style.minHeight = '48px';

			headerNav.style.paddingTop = '11px';

			let headerNavLinks 		= header.querySelectorAll('nav > ul > li');
			Object.keys( headerNavLinks ).forEach(function (i) {
				headerNavLinks[i].style.marginBottom = '1px';
			});
		}

		if( window.scrollY == 0) {
			headerLogo.setAttribute( 'src', '/dist/images/missionbio-logo-stacked.svg' );
			headerLogo.style.maxWidth = '100px';
			headerLogo.style.minWidth = '100px';

			headerNav.style.paddingTop = '35px';
			headerLogoContainer.style.minHeight = '100px';

			// Object.keys( headerNavLinks ).forEach(function (i) {
			// 	headerNavLinks[i].style.marginBottom = '20px';
			// });

		}


	}

	// only run the function if it's a homepage
	if( ( isHomepage || isTransNav ) && ( window.innerWidth > 960 ) ) {
		console.log('do that magic!');
		const header = document.getElementsByTagName('header')[0];
		const logo = document.querySelector('.header-site-logo img');
		const menuLinks = document.querySelectorAll('nav > ul > li.menu-item > a');
		const menuCTA = document.querySelector('li.menu-cta a');

		// invert the colors on mouse enter
		// menuCTA.addEventListener('mouseover', function(e) {
		// 	menuCTA.style.color = 'white';
		// 	menuCTA.style.backgroundColor = menuCTABacgroundColorHover;
		// });
		// invert the colors on mouse enter
		// menuCTA.addEventListener('mouseleave', function(e) {
		// 	menuCTA.style.color = menuCTAColor;
		// 	menuCTA.style.backgroundColor = menuCTABacgroundColor;
		// });

		window.addEventListener("scroll", function(e) {
			doHomepageNavScrollMagic( this, header, logo, menuLinks, menuCTA );
		});
	}


	// Run the menu shrink on all other pages
	if( ( window.innerWidth > 960 ) ) {

		const header = document.getElementsByTagName('header')[0];

		window.addEventListener("scroll", function(e) {
			doNavShrinkAndGrowOnScroll( header );
		});

	}
}




export default HomeScroll;
