const PlatformSolution = function() {

	const tapestriItems = document.getElementsByClassName('tapestri-item');

	Object.keys( tapestriItems ).forEach(function (i) {
		tapestriItems[i].addEventListener( 'mouseenter', function( e ) {

            //Manage hover state on tapestri items
            Object.keys( tapestriItems ).forEach(function (i) {
                tapestriItems[i].classList.remove('hover-state');
            });

			if( this.classList.contains('can-hover') ) {
				this.classList.add('hover-state');				
			}

            //hide any descriptions currently showing
            const tapestriItemsDescs = document.getElementsByClassName('tapestri-items-description');
            Object.keys( tapestriItemsDescs ).forEach(function (i) {
                tapestriItemsDescs[i].classList.add('hidden');
            });

            //get the associated description and show it
            const theDescription = document.getElementById(this.dataset.description);
			theDescription.classList.toggle('hidden');


		});
	});

}

export default PlatformSolution;
