import FlexSlider from 'flexslider/jquery.flexslider-min.js';

const Sliders = function () {

	jQuery(window).load(function() {

	  jQuery('#about-current-investors .flexslider').flexslider({
		animation: "slide",
		animationLoop: true,
		itemWidth: 200,
		itemMargin: 5,
		minItems: 2,
		maxItems: 4,
		move: 1,
		controlsContainer: jQuery(".custom-controls-container"),
		customDirectionNav: jQuery(".custom-navigation a")
	  });

	  jQuery('#homepage-testimonials .flexslider').flexslider({
		animation: "slide",
		animationLoop: true,
		controlNav: false,
		itemWidth: '100%',
		itemMargin: 'auto',
		minItems: 2,
		maxItems: 4,
		move: 1,
		controlsContainer: jQuery(".custom-controls-container"),
		customDirectionNav: jQuery(".custom-navigation a"),
		slideshowSpeed: 5000,
	  });
	});
}

export default Sliders;
