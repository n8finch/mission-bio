
const FastDomFuncs = function () {

    !function(t){"use strict";function e(){var e=this;e.reads=[],e.writes=[],e.raf=u.bind(t)}function n(t){t.scheduled||(t.scheduled=!0,t.raf(i.bind(null,t)))}function i(t){var e,i=t.writes,o=t.reads;try{o.length,r(o),i.length,r(i)}catch(t){e=t}if(t.scheduled=!1,(o.length||i.length)&&n(t),e){if(e.message,!t.catch)throw e;t.catch(e)}}function r(t){for(var e;e=t.shift();)e()}function o(t,e){var n=t.indexOf(e);return!!~n&&!!t.splice(n,1)}function s(t,e){for(var n in e)e.hasOwnProperty(n)&&(t[n]=e[n])}var u=t.requestAnimationFrame||t.webkitRequestAnimationFrame||t.mozRequestAnimationFrame||t.msRequestAnimationFrame||function(t){return setTimeout(t,16)};e.prototype={constructor:e,measure:function(t,e){var i=e?t.bind(e):t;return this.reads.push(i),n(this),i},mutate:function(t,e){var i=e?t.bind(e):t;return this.writes.push(i),n(this),i},clear:function(t){return o(this.reads,t)||o(this.writes,t)},extend:function(t){if("object"!=typeof t)throw new Error("expected object");var e=Object.create(this);return s(e,t),e.fastdom=this,e.initialize&&e.initialize(),e},catch:null};var exports=t.fastdom=t.fastdom||new e;"function"==typeof define?define(function(){return exports}):"object"==typeof module&&(module.exports=exports)}("undefined"!=typeof window?window:this);

    const equalHeights = function(elements) {
        let height = 0
        let elementHeights = []

        fastdom.measure(() => {
            [...elements].map( element => {

                // Remove any previous styling
                element.style.height = ""

                // Set Defaults
                let elHeight = {
                    full: 0,
                    padding: 0,
                    childHeight: 0,
                }

                // Get element's vertical padding
                elHeight.padding =
                    parseInt( window.getComputedStyle(element).paddingTop, 10) +
                    parseInt( window.getComputedStyle(element).paddingBottom, 10)

                // Get height of element's children
                for (let child of element.children ) {
                    if (window.getComputedStyle(child).position != 'absolute') {

                        elHeight.childHeight += child.clientHeight
                        elHeight.childHeight += parseInt(  window.getComputedStyle(child).marginTop, 10 )
                        elHeight.childrenHeight += parseInt( window.getComputedStyle(child).marginBottom, 10 )
                    }
                }

                // Get full height of element
                elHeight.full = elHeight.padding + elHeight.childHeight

                // Add height to Array
                elementHeights.push(elHeight.full)
            })

            fastdom.mutate(()=> {
                height = elementHeights.reduce( function(acc,curr) {
                    if ( curr > acc ) acc = curr
                    return acc
                }, height)
                Array.from(elements).map( element => element.style.height = height + 'px')
            })
        })
    }

    let resourceItems = document.getElementsByClassName("equal-heights");
    equalHeights(resourceItems);
    window.addEventListener("resize", equalHeights.bind(this,resourceItems));
    equalHeights(resourceItems);
    window.addEventListener("resize", equalHeights.bind(this,resourceItems));

}

export default FastDomFuncs;
