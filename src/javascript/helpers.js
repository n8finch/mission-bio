const Helpers = function() {

	// const subMenuLinks = document.querySelectorAll('.sub-menu > li');
	//
	// //add circle to current active menu
	// for (var i = 0, len = subMenuLinks.length; i < len; i++) {
	//
	// 	// if it's the active menu item, add the red dot
	// 	if( subMenuLinks[i].classList.contains('current-menu-item') ) {
	//
	// 		subMenuLinks[i].classList.add('red-dot');
	// 		subMenuLinks[i].innerHTML = '<span class="uk-icon-circle"></span>' + subMenuLinks[i].innerHTML;
	//
	// 	}
	//
	// 	// do the mouse over functionality
	// 	subMenuLinks[i].addEventListener( 'mouseenter', function( e ) {
	//
	// 		if( this.classList.contains('red-dot') ) {
	// 			return;
	// 		} else {
	// 			this.classList.add('red-dot');
	// 			this.innerHTML = '<span class="uk-icon-circle"></span>' + this.innerHTML;
	// 		}
	// 	});
	//
	// 	// do mouse leave functionality
	// 	subMenuLinks[i].addEventListener( 'mouseleave', function( e ) {
	//
	// 		if( this.classList.contains('current-menu-item') ) {
	// 			return;
	// 		}
	//
	// 		if( this.classList.contains('red-dot') ) {
	// 			this.classList.remove('red-dot');
	// 			this.innerHTML = this.innerHTML.replace('<span class="uk-icon-circle"></span>', '');
	// 		}
	// 	});
	// }



	/**
	 * Move all modals, whenever they're found, to the footer, so that
	 * they show up on top of other elements.
	 */
	const pageModals = document.querySelectorAll('div.uk-modal');

	pageModals.forEach( function( item, index ) {
		const footer = document.querySelector('footer');
		footer.after(item);
	});

}

export default Helpers;
