import EqualHeights from 'equal-heights';
import Helpers from 'helpers';
import HelloBar from 'hello-bar';
import PressPage from 'press-page';
import Sliders from 'slides-and-carousels';
import HomeScroll from 'home-scroll';
import PlatformSolution from 'platform-solution';
import ResourcesScroll from 'resources-scroll';
import ResourcesAccordion from 'resources-accordion';

(function() {

	// EqualHeights();
	Helpers();
	HelloBar();
	HomeScroll();
	Sliders();
	PlatformSolution();
	ResourcesScroll();
	ResourcesAccordion()

})();
