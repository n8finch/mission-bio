import gulp from "gulp"
import { sassCritical, sassDev, sassProdCritical, sassProd } from "./config/gulp/sass.js"
import images from "./config/gulp/images.js"
import server from "./config/gulp/server.js"

gulp.task( sassDev )
gulp.task( sassProd )
gulp.task( images )

const dev = gulp.series( gulp.parallel(images, sassCritical, sassProdCritical, sassDev), server )
const prod = gulp.series( sassProdCritical, sassProd )

dev.description = "Build and launch local site"
prod.description = "Build assets for production site"

export { prod }
export default dev
