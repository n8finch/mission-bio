import gulp from 'gulp'
import imagemin from 'gulp-imagemin'
import newer from 'gulp-newer'
import { config } from './config'

const { source, dest } = config.images

function images() {
	return gulp
		.src( source )
		.pipe( newer( source) )
		.pipe( imagemin([
				imagemin.gifsicle(),
				imagemin.jpegtran({progressive:true}),
				imagemin.optipng({optimizationLevel: 5}),
				imagemin.svgo({
					plugins: [
						{removeViewBox:false},
						{cleanupIDs: false},
						{removeUselessDefs: false},
					]
				})
			]) 
		)
		.pipe( gulp.dest(dest) )
}

images.displayName = "images"
images.description = "Image/icon optimizer"

export default images