import path from 'path'

const root = path.join( __dirname, '..', '..' )
const src = path.join( root, 'src' )
const dist = path.join( root, 'dist' )

let config = {
	sass: {
		source: [
			path.join( src, 'css', 'app.scss' ),
			path.join( src, 'css', 'critical.scss' ),
		],
		dest: {
			app: path.join( dist, 'css' ),
		},
		watch: path.join( src, 'css/**/*' ),

		browsers: [],

		options: {
			dev: {
				indentedSyntax: false,
				indentType: 'tab',
				indentWidth: 1,
				sourceComments: true,
				outputStyle: 'expanded',
			},
			prod: {
				indentedSyntax: false,
				sourceComments: false,
				outputStyle: 'compressed'
			},
		},
	},

	postCSS: {
		plugins: {
			dev: [
				require('postcss-text-remove-gap')(),
				require('postcss-flexbugs-fixes')(),
				require('postcss-cssnext')([ 'last 2 versions', 'safari >=8', 'ie >= 10', 'ios >= 8' ])
			],
			prod: [
				require('postcss-text-remove-gap')(),
				require('postcss-flexbugs-fixes')(),
				require('postcss-cssnext')([ 'last 2 versions', 'safari >=8', 'ie >= 10', 'ios >= 8' ]),
				require('cssnano')({
					autoprefixer: false,
					zindex: false,
				}),
			],
		},

	},

	images: {
		source: path.join( src, 'images/**/*' ),
		dest: path.join( dist, 'images' ),
	},

	markup: path.join( root, 'content', 'themes', 'mbio-beans/**/*' ),

	server: {
		host: 'localhost',
		target: 'mbio.test',
		port: 8080,
	}
}

export { config }
