import gulp from "gulp"
import postcss from "gulp-postcss"
import sass from "gulp-sass"
import sourcemaps from "gulp-sourcemaps"
import rename from "gulp-rename"
import server from "./server"
import { config } from "./config"

const { source, dest, options } = config.sass
const { plugins } = config.postCSS

function sassCritical() {
	return gulp
		.src( source[1], { base: "src/css", allowEmpty: true } )
		.pipe( sourcemaps.init() )
		.pipe( sass(options.dev) )
		.on( "error", sass.logError )
		.pipe( postcss(plugins.dev,[]) )
		.pipe( sourcemaps.write() )
		.pipe( gulp.dest(dest.app) )
		// .pipe( server.reload({stream: true}) )
}

sassCritical.displayName = "sass:critical"
sassCritical.description = "Sass/SCSS compiler for local/staging environments."

function sassDev() {
	return gulp
		.src( source, { base: "src/css", allowEmpty: true } )
		.pipe( sourcemaps.init() )
		.pipe( sass(options.dev) )
		.on( "error", sass.logError )
		.pipe( postcss(plugins.dev,[]) )
		.pipe( sourcemaps.write() )
		.pipe( gulp.dest(dest.app) )
		// .pipe( server.reload({stream: true}) )
}

sassDev.displayName = "sass:dev"
sassDev.description = "Sass/SCSS compiler for local/staging environments."

function sassProdCritical() {
	return gulp
		.src( source[1], { base: "src/css", allowEmpty: true } )
		.pipe( sass(options.prod) )
		.on( "error", sass.logError )
		.pipe( postcss(plugins.prod,[]) )
		.pipe( rename("critical.min.css"))
		.pipe( gulp.dest(dest.app) )
}

sassProdCritical.displayName = "sass:ProdCritical"
sassProdCritical.description = "Critical Sass/SCSS compiler for production environments."

function sassProd() {
	return gulp
		.src( source[0], { base: "src/css", allowEmpty: true } )
		.pipe( sass(options.prod) )
		.on( "error", sass.logError )
		.pipe( postcss(plugins.prod,[]) )
		.pipe( rename("app.min.css"))
		.pipe( gulp.dest(dest.app) )
}

sassProd.displayName = "sass:prod"
sassProd.description = "Sass/SCSS compiler for production environments."

export { sassCritical, sassDev, sassProdCritical, sassProd }
