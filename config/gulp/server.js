import browserSync from 'browser-sync'
import gulp from 'gulp'
import path from 'path'
import webpack from 'webpack'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'
import { config } from './config'
import webpackDevConfig from "../webpack/config.dev"
import webpackProdConfig from "../webpack/config.prod"

import { sassDev } from "./sass"
import images from "./images"

const { host, target, port } = config.server
const bundler = webpack( webpackDevConfig )
const devServer = browserSync.create()
const reload = devServer.reload()

function server() {

	// Set Middleware Options
	let middleware = [
		webpackDevMiddleware( bundler, {
			publicPath: webpackDevConfig.output.publicPath,
			stats: { colors: true },
			noInfo: false,
			quiet: false,
			host,
			port,
		} )
	];


	// Pass Server Options to Browsersync Instance
	devServer.init({
		proxy: {
			ws: false,
			target,
			middleware,
		},
		host,
		port,
		open: false,
		ghostMode: false,
		notify: false,
		logLevel: "info",
	})

	// Set Watch Tasks for Live Reloads/Injections
	gulp.watch( config.sass.watch, gulp.series("sass:dev") )
	gulp.watch( config.images.source, gulp.series("images") ).on( "change", devServer.reload )
	gulp.watch( config.markup.watch ).on( "change", devServer.reload )
	bundler.plugin( "done", stats => devServer.reload() )
}

export default server