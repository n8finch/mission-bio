const path = require( 'path' )
const webpack = require( 'webpack' )
const commonConfig = require( './config.common.js' )
const root = path.join( __dirname, '..', '..' )

const prodConfig = {
	
	output: {
		path: path.resolve( root, 'dist', 'javascript' ),
		filename: '[name].min.js',
		publicPath: '/dist/javascript',
	},
	cache: true,
	plugins: [
		new webpack.optimize.UglifyJsPlugin({
			compress: { drop_console: true },
			mangle: { keep_fnames: true },
		}),
		new webpack.NamedModulesPlugin(),
	],
	performance: {
		hints: 'warning',
	},
}

module.exports = Object.assign( {}, commonConfig, prodConfig )