const path = require( 'path' )
const webpack = require( 'webpack' )
const commonConfig = require( './config.common.js' )
const root = path.join( __dirname, '..', '..' )

const devConfig = {
	devtool: 'inline-source-map',
	output: {
		path: path.resolve( root, 'dist', 'javascript' ),
		filename: '[name].js',
		publicPath: '/dist/javascript',
	},
	cache: true,
	plugins: [
		new webpack.NoEmitOnErrorsPlugin(),
		new webpack.NamedModulesPlugin(),
	],
	devServer: {
		quiet: false,
		clientLogLevel: 'error',
		compress: true,
		stats: 'normal',
	},
}

module.exports = Object.assign( {}, commonConfig, devConfig )