const path = require( 'path' )
const webpack = require( 'webpack' )

const root = path.join( __dirname, '..', '..' )
const context = path.join( root, 'src', 'javascript' )

module.exports = {
	context,

	entry: {
		app: [ './app' ],
	},

	module: {
		rules: [
			{
				test: /\.jsx?$/,
				use: { loader: "babel-loader" },
				include: [
					path.resolve( root, "src", "javascript" ),
					path.resolve( "node_modules", "when-dom-ready" ),
				],
				exclude: /(node_mdules)/,
			},
			{
				test: /\.json$/,
				use: { loader: "json-loader" },
			},
			{
				test: /isotope\-|fizzy\-ui\-utils|desandro\-|masonry|outlayer|get\-size|doc\-ready|eventie|eventemitter|fastdom/,
				loader: 'imports-loader?define=>false&this=>window',
			},
		],
	},

	resolve: {
		modules: [
			path.resolve( root, 'src', 'javascript' ),
			//path.resolve( jsContext, "your-directory" ),
			'node_modules',
		],
		alias: {
			// yourdirectory: path.resolve( jsContext, "your-directory" ),
		},
		extensions: [ '.js', '.jsx', '.es6' ],
	},

	target: 'web',

	node: {
		fs: 'empty',
	},
}
