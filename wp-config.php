<?php

/*--------------------------------------------------------*\
	Environment Config Files
\*--------------------------------------------------------*/
$env_check = "dev";

if ( file_exists(dirname(__FILE__) . '/config/wp/config.db.php') ) {
	include( dirname(__FILE__) . '/config/wp/config.db.php' );
}
else {
	die( "Site is still being provisioned; Missing database config file." );
}

/*--------------------------------------------------------*\
	Charset && Collate
\*--------------------------------------------------------*/

if ( ! defined('DB_CHARSET') ) {
	define( 'DB_CHARSET', 'utf8' );
}

if ( ! defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/*--------------------------------------------------------*\
	Keys && Salts
\*--------------------------------------------------------*/

define( 'AUTH_KEY',         '#>aWG3z#^3|]g8$xK?&L$$!Y+6~|5 zRTP^Ogt)Pb41O9rzm{Iyd6+e3~t<4bx5(' );
define( 'SECURE_AUTH_KEY',  'D1c<!Bfi[AuQEI4|vCgzJ+vFA|Q{|=QJPoZ?ML!{g_/@,[.kA|KP{,uW&K6~;QxL' );
define( 'LOGGED_IN_KEY',    '{?X!|K= sMNb?j:b*W75h;:E+(:4Z?ew;F<GwR/P&ku8tDaRCD^lrF`2j&+hYM<i' );
define( 'NONCE_KEY',        '>/s3v>m^rk?pr:5/cB&89z,,XDfH:{1EbVJf9(~uj;bYgIaq=ZR=+^a>;C(`U=e(' );
define( 'AUTH_SALT',        '>w2(e|?eRB_.4VWo,UTk+8|-l/Pkb+ci#xx?4s7)-LPot+-S<FJx$mU$S=F;?Sr5' );
define( 'SECURE_AUTH_SALT', 'T*Mi/ +J^kTbK#&+q]`Nv!=2vHb%n7k(@-Q84TC:o&=WE7rTKe^79KFhYsJ)oa55' );
define( 'LOGGED_IN_SALT',   'qAn}1}kIpt<-~E c):0/dYzqIdMw|[r8=XN!-P}|vs2z^7J]c7c ZWfXba_,NgG%' );
define( 'NONCE_SALT',       'q/F:?6>H/ 7nV)3L|3pKr4+7eRhG}611.&T00/Jl[Buc1Irj!Wx?uL;wq)gTQ/G<' );

/*--------------------------------------------------------*\
	DB Prefix
\*--------------------------------------------------------*/

$table_prefix = "mbio_";

/*--------------------------------------------------------*\
	Language
\*--------------------------------------------------------*/

define( 'WPLANG', '' );

/*--------------------------------------------------------*\
	Custom Paths
\*--------------------------------------------------------*/

if ( ! defined('WP_SITEURL') ) {
	define( 'WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp' );
}

if ( ! defined('WP_HOME') ) {
	define( 'WP_HOME', 'http://' . $_SERVER['SERVER_NAME'] );
}

if ( ! defined('WP_CONTENT_DIR') ) {
	define( 'WP_CONTENT_DIR',  dirname(__FILE__) . '/content' );
}

if ( ! defined('WP_CONTENT_URL') ) {
	define( 'WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/content' );
}

/*--------------------------------------------------------*\
	Debug
\*--------------------------------------------------------*/

if ( "production" === $env_check ) {
	define('WP_DEBUG', false);
} else {
	define( 'WP_DEBUG', true );
}


require_once( ABSPATH . 'wp-settings.php' );
