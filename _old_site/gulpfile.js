'use strict'

const gulp =    require('gulp');
const concat =  require('gulp-concat'); //used for concatinating js files
const uglify =  require('gulp-uglify'); //used to minify js files
const rename =  require('gulp-rename'); //used to rename files
const sass =    require('gulp-sass');	//used to compile sass
const maps =    require('gulp-sourcemaps'); //used to make sourcemaps files for js, css, scss,less
const cssnano = require('gulp-cssnano'); // used to minify css
const del =     require('del'); // used to delete files for clean up
const autoprefixer = require('gulp-autoprefixer'); //used to auto add vendor prefixes to css
const browserSync =  require('browser-sync'); //reloads browser after saving a change to a file
const twig =         require('gulp-twig'); //templates html
const prettify =     require('gulp-prettify'); //properly indents html files

// js files to be concatinated in this order
var vendorScripts = [
		'node_modules/jquery/dist/jquery.min.js',
		'./src/assets/js/vendors/bootstrap.js',
		'./src/assets/js/vendors/img-load.js',
		'./src/assets/js/vendors/bootstrap-datepicker.js',
		'./src/assets/js/vendors/TweenMax.min.js',
		'./src/assets/js/vendors/ScrollMagic.js',
		'./src/assets/js/vendors/animation.gsap.js',
		'./src/assets/js/vendors/debug.addIndicators',
		'./src/assets/js/vendors/autosize.js',
		'./src/assets/js/vendors/resize-event.js'
		];
var mainScripts = [
	'./src/assets/js/main/header_nav_ctrls.js',
	'./src/assets/js/main/vertical-center.js',
	'./src/assets/js/main/date-picker-ctrl.js',
	'./src/assets/js/main/fix-header.js',
	'./src/assets/js/main/mb-selector.js',
	'./src/assets/js/main/scroll-ctrl.js',
	'./src/assets/js/main/autosize-ctrl.js',
	'./src/assets/js/main/smooth-scroll.js',
	'./src/assets/js/main/form-handling-contact.js',
	'./src/assets/js/main/parallax-ctrl.js',
	'./src/assets/js/main/mb-popup.js',
	// './src/assets/js/main/hero-intro-animation.js',
	'./src/assets/js/main/video-modal.js',
	'./src/assets/js/main/video-modal-link.js',
	'./src/assets/js/main/views-modal-form-handling.js',
	'./src/assets/js/main/views-modal.js',
	'./src/assets/js/main/views-modal-ctrl.js'
];

var scriptFeed = vendorScripts.concat(mainScripts);

var supported = [
	'last 5 versions',
	'safari >= 8',
	'ie >= 9',
	'ff >= 20',
	'ios 6',
	'android 4'
];

// Compile Twig templates to HTML
gulp.task('template', function() {
	return gulp.src('src/templates/*.html') // run the Twig template parser on all .html files in the "src" directory
	.pipe(twig())
	// .pipe(prettify())
	.pipe(gulp.dest('./dist')) // output the rendered HTML files to the "dist" directory
});

// Compile Twig templates to HTML
gulp.task('htmlsync', ['template'], function() {
	return gulp.src('src/templates/*.html') // run the Twig template parser on all .html files in the "src" directory
	.pipe(browserSync.stream());
});

// concatinates js from the scripts var into one file app.js,
// minifys app.js into app.min.js,
// then writes the source maps,
// places both files in ./js,
// and reloads the browser
gulp.task('minifyScripts', function() {
	return gulp.src(scriptFeed)
	.pipe(maps.init())
	.pipe(concat('app.js'))
	.pipe(uglify())
	.pipe(rename("app.min.js"))
	.pipe(maps.write('./'))
	.pipe(gulp.dest('./dist/assets/js'))
	.pipe(browserSync.stream());
});

// same as minifyScripts minus sourcemaps and browsersync
gulp.task('minifyScripts-noMaps', function() {
	return gulp.src(scriptFeed)
	.pipe(concat('app.js'))
	.pipe(uglify())
	.pipe(rename("app.min.js"))
	.pipe(gulp.dest('./dist/assets/js'))
});


// compiles sass from scss/main.scss to main.css
// adds prefixes with auto prefixer to css
// minifies css to main.min.css
// writes source maps
// places both in dist/css
gulp.task('minifyCss', function() {
	return gulp.src('src/assets/scss/main.scss')
	.pipe(maps.init())
	.pipe(sass())
	.pipe(cssnano({
		autoprefixer: {browsers: supported, add: true}
	}))
	.pipe(rename("main.min.css"))
	.pipe(maps.write('./'))
	.pipe(gulp.dest('./dist/assets/css'))
	.pipe(browserSync.stream());
});

//same ass minifyCss minus sourcemaps and browsersync
gulp.task('minifyCss-noMaps', function() {
	return gulp.src('src/assets/scss/main.scss')
	.pipe(sass())
	.pipe(cssnano({
		autoprefixer: {browsers: supported, add: true}
	}))
	.pipe(rename("main.min.css"))
	.pipe(gulp.dest('./dist/assets/css'))
});


// starts development server at localhost:3000
// watches html, js, and scss and runs associated tasks
gulp.task('watchFiles',['build'], function() {
	browserSync.init({
        server: "./dist"
    });
	gulp.watch('./src/assets/scss/**/*.scss', ['minifyCss']);
	gulp.watch('./src/assets/js/**/*.js', ['minifyScripts']);
	gulp.watch('./src/templates/**/*.html', ['htmlsync']);
	// gulp.watch('./src/php/**/*.php', ['getphp']);		// watch and update templates
});

// builds the dist directory and by running associated tasks
// that place their contents in dist, and by placing the folders and 
// files returned from gulp.src into dist 
gulp.task('build', ['template', 'minifyScripts', 'minifyCss'], function() {
	return gulp.src(["src/assets/img/**", 'src/assets/pdf/**', './src/php/**', './src/assets/fonts/**', './src/assets/css/**'], { base: './src'})
	.pipe(gulp.dest('dist'));
});

gulp.task('prod', ['template', 'minifyScripts-noMaps', 'minifyCss-noMaps'], function() {
	return gulp.src(["src/assets/img/**", 'src/assets/pdf/**', './src/php/**', './src/assets/fonts/**', './src/assets/css/**'], { base: './src'}) 
	.pipe(gulp.dest('dist'));
});

// deletes all folders created by gulp tasks
gulp.task('clean', function(){
	del(['dist']);
});

// default task is set to start the watch listeners
gulp.task('default', [], function() {
	gulp.start(['watchFiles']);
});

