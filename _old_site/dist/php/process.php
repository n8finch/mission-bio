<?php
// process.php

require("phpmailer/class.phpmailer.php");
require("phpmailer/class.smtp.php");

$data           = array();      // array to pass back data

$fullname = trim(filter_input(INPUT_POST, "fullname", FILTER_SANITIZE_STRING));
$institutionName = trim(filter_input(INPUT_POST, "institution", FILTER_SANITIZE_STRING));
$email = trim(filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL));
$phone = trim(filter_input(INPUT_POST, "phone", FILTER_SANITIZE_STRING));
$message = trim(filter_input(INPUT_POST, "message", FILTER_SANITIZE_STRING));

//$receiving_email = "adam@audacityhealth.com";
$receiving_email = "info@missionbio.com";
 
$messageBody .= "Name: ". $fullname;
$messageBody .= "\r\n";
$messageBody .= "Institution: ". $institutionName;
$messageBody .= "\r\n";
$messageBody .= "Email: ". $email;
$messageBody .= "\r\n";
$messageBody .= "Phone: ". $phone;
$messageBody .= "\r\n";
$messageBody .= "Message: ". $message;

$data['message'] = $messageBody;

// return a response ===========================================================

    $mail = new PHPMailer;
    if (!$mail->ValidateAddress($email)){
        $invalid_email = true;
        $data['success'] = false;
        $data['errors']  = "Invalid Email";
    }else {
        //$mail->SMTPDebug = 3;
//        $mail->isSMTP();  // Set mailer to use SMTP
        $mail->Host = "smtp.mailgun.org";  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;  // Enable SMTP authentication
        $mail->Username = 'postmaster@mg.audacityhealth.com';    // SMTP username
        $mail->Password = '8z039t7wc1w1';   // SMTP password
        $mail->SMTPSecure ='ssl';
        $mail->Port = 465;
        $mail->setFrom($receiving_email, $receiving_email);
        $mail->addAddress($receiving_email , 'Mission Bio');     // Add a recipient
        $mail->isHTML(false);
        $mail->Subject = 'Mission Bio Website Inquiry';
        $mail->Body    = $messageBody;
        if(!$mail->send()) {
            $data['success'] = false;
            $data['errors']  = $mail->ErrorInfo;
        }else{
            $data['success'] = true;
            $data['status'] = 'Success!';
        }
    }

echo json_encode($data);