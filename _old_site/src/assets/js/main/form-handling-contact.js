$(document).ready(function() {
    
    // process the form
    if($('#contact-form').length){
        contact_form();
    }
    function contact_form(){
        var emailCheck = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var formTime;
        var submitTime;
        var formBool = true;
        //storing time of first input click;
        $('#contact-form input, #contact-form select, #contact-form textarea').on('focus', function(){
            if(formBool){
                formTime = new Date().getTime();
                formBool = false;
            }
        });
        $('#contact-form').on("submit", function(e) {
            e.preventDefault();
            $('.error').remove();
            $('.input-error').removeClass('input-error');
            var errors = {};
            submitTime = new Date().getTime();
            var elapsedTime = submitTime - formTime;
            if (elapsedTime < 1500){
                $('#contact-form').addClass('robo-fill');
                return false;
            }

            var form = $(this);
            var payload = form.serialize();
            var $fullname = $('#contact-form input[name=fullname]');
            var $institutionName = $('#contact-form input[name=institution]');
            var $email = $('#contact-form input[name=email]');
            var $phone = $('#contact-form input[name=phone]');
            var $message = $('#contact-form textarea[name=message]');

            // payload += "&hasjs=true"; // used to check if js is used to submit the form

            // error handling
            if($fullname.val() == ''){
                errors.fullname = "Name is required";
                errors.status = true;
            }
            // if($institutionName.val() == ''){
            //     errors.institutionName = "Institution Name is required";
            //     errors.status = true;
            // }
            if($email.val() == ''){
                errors.email = "Email is required";
                errors.status = true;
            }
            if (!emailCheck.test($email.val())) {
                errors.email = "Valid email is required";
                errors.status = true;
            }
            // if($phone.val() == ''){
            //     errors.phone = "Phone is required";
            //     errors.status = true;
            // }
            if($message.val() == ''){
                errors.message = "Message is required";
                errors.status = true;
            }

            if (errors.status) {
                $('.required-prompt').append("<span class='error'>&emsp; – please fill out required fields</span>");
                if(errors.fullname){
                    $fullname.addClass('input-error');
                    $fullname.parents(".form-item").append("<span class='error'>" + errors.fullname + "</span>");
                }
                // if(errors.institutionName){
                //     $institutionName.addClass('input-error');
                //     $institutionName.parents(".form-item").append("<span class='error'>" + errors.institutionName + "</span>");
                // }
                if(errors.email){
                    $email.addClass('input-error');
                    $email.parents(".form-item").append("<span class='error'>" + errors.email + "</span>");
                }
                // if(errors.phone){
                //     $phone.addClass('input-error');
                //     $phone.parents(".form-item").append("<span class='error'>" + errors.phone + "</span>");
                // }
                if(errors.message){
                    $message.addClass('input-error');
                    $message.parents(".form-item").append("<span class='error'>" + errors.message + "</span>");
                }
                return
            }

            // process the form
            $.ajax({
                type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url         : '../../php/process.php', // the url where we want to POST
                data        : payload, // our data object
                dataType    : 'json', // what type of data do we expect back from the server
                encode          : true
            })
            // using the done promise callback
            .done(function(data) {
                console.log('message sent');
                console.log($('#contact-form').parent());
                $('#contact-form').parent().addClass('message-sent');
            }).fail(function (data) {
                console.log('message not sent');
            });
        });
    }
});