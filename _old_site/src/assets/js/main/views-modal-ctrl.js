modalTriggers();

function modalTriggers(){
    $('[data-view-trigger]').on('click', function(){
        var $this = $(this);
        var view = $this.attr('data-view-trigger');
        var $view_container = $("[data-modal-view="+view+"]");
        var leadCheck = localStorage.getItem("leadCheck");
        var videos = {
            "video-1": "<iframe width='985' height='675' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='http://cnpg.comparenetworks.com/343899-Pioneering-Single-Cell-DNA-Sequencing/'></iframe>",
            "video-2": "<iframe width='985' height='675' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='http://cnpg.comparenetworks.com/343937-Tapestri-Precision-Genomics-Platform/'></iframe>",
            "video-3": "<iframe width='985' height='675' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='http://cnpg.comparenetworks.com/343939-High-throughput-single-cell-DNA-sequencing-of-AML-tumors/'></iframe>",
            "video-4": "<iframe width='985' height='675' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='http://cnpg.comparenetworks.com/343942-Clonal-Evolution-and-Changes-of-Gene-Mutations/'></iframe>"
        }
         

        var current_video = videos['video-'+view];
        var removeVideo = function(){
            $view_container.children('.video-wrapper2').children(".video-container2").children("iframe").remove();
        }

        if(leadCheck != "true"){
            mainModal.openModal(5);
            // lead_form is the yes form 
            lead_form(function(){
                mainModal.openModal(view, removeVideo);  
                $view_container.children('.video-wrapper2').children(".video-container2").append(current_video);
                localStorage.setItem('leadCheck', 'true');
            });
            
        }else {
            mainModal.openModal(view, removeVideo); 
            $view_container.children('.video-wrapper2').children(".video-container2").append(current_video);
        }
       
    });
}

