
    function MainModal(modal){
        this.$main_modal = $(modal);
        this.cb = undefined
        this.init = function(){
            this.setModalCloseEvents();
        }

        return this.init();
    }

    MainModal.prototype.openModal = function(view, cb){
        this.cb = cb;
        var $main_modal = $('.main-modal');
        setTimeout(function(){
            $main_modal.addClass('modal-active');
            setTimeout(function(){
                $main_modal.addClass('show-modal');
            },50)
        },100);
        this.setveiw(view);
    }

    MainModal.prototype.setveiw = function(view){
        var $view = $("[data-modal-view="+view+"]")
        var $view_container = $(".modal-view-container");
        var modalview = $(".modal-view");

        // logic preformed if a veiw is currenly showing
        if($(".view-active").length){
            var active = $(".view-active");
            active.removeClass('show-view');
            setTimeout(function(){
                active.removeClass('view-active');
                modalview.removeClass("view-active");
                $view.addClass('view-active');
                setTimeout(function(){
                    var setheight = $view.outerHeight();
                    $view_container.animate({
                        "height": setheight
                    })
                },100);
                setTimeout(function(){
                    $view.addClass("show-view"); 
                }, 300)  
            },300);
        // logic preformed for the first view to show
        } else {
            modalview.removeClass("view-active");
            $view.addClass('view-active');
            setTimeout(function(){
                var setheight = $view.outerHeight();
                $view_container.css({
                    "height": setheight
                }, 300);
            },100);
            $view.addClass("show-view"); 
        }

        window.addEventListener('resize', function(){
            if($(".view-active").length){
                $view_container.css({
                    "height": $view.outerHeight()
                });
            }
        });
    }

    MainModal.prototype.setModalCloseEvents = function(){
        var self = this;
        self.$main_modal.children('.main-modal__container').children('.main-modal-content').children('.close-modal').on('click', function(e){
            self.$main_modal.removeClass('show-modal');
            setTimeout(function(){
                self.$main_modal.removeClass('modal-active');
            }, 400);
            if(self.cb != undefined){
                return self.cb();
            }
        });

        $(document).on('click touchend', self.$main_modal, function(e){
            if($(e.target).hasClass('main-modal')) {
                self.$main_modal.removeClass('show-modal');
                setTimeout(function(){
                    self.$main_modal.removeClass('modal-active');
                }, 400);
                if(self.cb != undefined){
                    return self.cb();
                }
            }
        }); 
    }


    if($(".main-modal").length){
        var mainModal = new MainModal('.main-modal'); 
    }
