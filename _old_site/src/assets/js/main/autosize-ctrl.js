(function(){
    //add autosizing to the textarea
    $('.autosize').each(function(){
        autosize(this);
    });

    formLabelCtrl();
    function formLabelCtrl (){
        $('.form-item .form-control').on('focus', function(){
            placeLabel(this);
         });
     
        $('.form-item .form-control').on('blur', function(){
            removeLabelClass();
        })
          
        $('.form-item .form-control').on('change', function(){
            var $this = $(this);
            if(!$this.hasClass('raise-label') && this.value != undefined && this.value != ''){
                $this.parents('.form-item').addClass('raise-label');
            }
        })
     
     
        function placeLabel(el){
            var $this = $(el);
            if(!$this.hasClass('raise-label')){
                $this.parents('.form-item').addClass('raise-label');
            }
        }
     
        function removeLabelClass(){
            var formItems = $('.form-item .form-control');
            formItems.each(function(item){
                var $this = $(this);
                if(this.value == undefined || this.value == ''){
                    $this.parents('.form-item').removeClass('raise-label');
                }
            });
        }
    
        autocompleteCheck();
        function autocompleteCheck(){
            var count = 0;
            var $formItems = $('.form-item .form-control')
            var check = setInterval(function(){
                $formItems.each(function(){
                    var $this = $(this);
                    if(!$this.hasClass('raise-label') && this.value != undefined && this.value != ''){
                        $this.parents('.form-item').addClass('raise-label');
                    }
                });
                count++;
                if(count >= 20){
                    clearInterval(check);
                }
            },500);
        } 
    }


})();