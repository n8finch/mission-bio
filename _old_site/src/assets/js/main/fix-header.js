(function(){
   
    if($('.home-template').length){
        setHeader();
    }

    function setHeader(){
        if($('.header-placeholder').length){
            var $headerPlaceHolder = $('.header-placeholder');
            var $header = $('.header');
            $(window).on('scroll', function(e){
                var $this = $(this);
                if($this.scrollTop() >= $headerPlaceHolder.offset().top){
                    $header.addClass("fixed");
                }else {
                    $header.removeClass("fixed");
                }
            });
        }
    }

})();


