(function(){

    if($('.intro-scene').length){
        var data = sessionStorage.getItem('intoAnimationCheck');
        if(data != 'animation-seen' && window.innerWidth > 1024){
            heroIntroAnimation();
            sessionStorage.setItem('intoAnimationCheck', 'animation-seen');
        } else {
            removeIntroScene();
        }
        
    }
    function heroIntroAnimation(){
        var introAnimation = new TimelineMax()
        .to('#hero-overlay', 0.5, {opacity: 0})

        .fromTo('#intro-scene__bar-1', 0.25, {opacity: 0},{opacity: 1})
        .to('#intro-scene__bar-1', 0.4, {opacity: 0.6})
        .fromTo('#intro-m-line-1', 0.4, {opacity: 0},{opacity: 1},'=-0.65')

        .fromTo('#intro-scene__bar-2', 0.25, {opacity: 0},{opacity: 1}, '=-0.2')
        .to('#intro-scene__bar-2', 0.4, {opacity: 0.6})
        .fromTo('#intro-m-line-2', 0.4, {opacity: 0},{opacity: 1},'=-0.65')

        .fromTo('#intro-scene__bar-3', 0.25, {opacity: 0},{opacity: 1}, '=-0.2')
        .to('#intro-scene__bar-3', 0.4, {opacity: 0.6})
        .fromTo('#intro-m-line-3', 0.4, {opacity: 0},{opacity: 1},'=-0.65')

        .fromTo('#intro-scene__bar-4', 0.25, {opacity: 0},{opacity: 1}, '=-0.2')
        .to('#intro-scene__bar-4', 0.4, {opacity: 0.6})
        .fromTo('#intro-h-line-1', 0.4, {opacity: 0},{opacity: 1},'=-0.65')
        
        .fromTo('#intro-scene__bar-5', 0.25, {opacity: 0},{opacity: 1}, '=-0.2')
        .to('#intro-scene__bar-5', 0.4, {opacity: 0.6})
        .fromTo('#intro-h-line-2', 0.4, {opacity: 0},{opacity: 1},'=-0.65')

        .fromTo('#intro-scene__bar-6', 0.25, {opacity: 0},{opacity: 1}, '=-0.2')
        .to('#intro-scene__bar-6', 0.4, {opacity: 0.6})
        .fromTo('#intro-period', 0.4, {opacity: 0},{opacity: 1},'=-0.65')

        .to('.intro-mb-logo-red', 1, {fill: "rgb(238, 61, 77)"})
        .to('.intro-mb-logo-blue', 1, {fill: "rgb(69, 195, 226)"}, '=-1')
        .to('#hero-overlay', 1, {opacity: 0.9}, '=-1')
        .to('.intro-scene', 1, {opacity: 0, onComplete: removeIntroScene, delay: 0.2});
    }

    function removeIntroScene(){
        $('.intro-scene').css({display: 'none'});
        $('body').addClass('intro-finished');
    }

})();