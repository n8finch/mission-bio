

/*  smooth scrolling  ******************************************************************
 **************************************************************************************/


smoothScroll();

function smoothScroll() {
    var $header = $('.header');
    var $navLink = $('.menu-item .scroll-toggle[href*="#"]:not([href="#"])');
    var headerHeight = $header.outerHeight();
    var $markers = $('.id-marker');

    function setMarkerOffset() {
        headerHeight = $header.outerHeight();
        $markers.each(function () {
            var $marker = $(this);
            $marker.css('top', headerHeight * -1);
        });
    }

    setMarkerOffset();

    $(window).on('resize', setMarkerOffset);

    $navLink.on('click', function (e) {
        if($('.home-template').length){
            e.preventDefault();
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            var targetOffset = target.offset().top + 2;
            if (target.length) {
                $('html, body').animate({scrollTop: targetOffset}, 1000);
                return false;
            }
        }
    });
}

activeItem();

function activeItem() { 
    var hh = $('.header').outerHeight();
    $('.menu-item a').each(function(i){
        var $this = $(this);
        var href = $this.attr('href');

        var Idtarget = href.slice(href.indexOf("#"));
        // Ensure the href is an anchor in the page
        if(Idtarget != undefined && Idtarget[0] == '#' ) { 
            var id = $(Idtarget);
            // console.log(id);
            if(id.position() !== undefined){
                $(window).on('scroll', function(){
                    if(window.innerWidth > 1024){
                        var scrollP = $(document).scrollTop();
                        var sectionPos = id.offset().top; //distance of the id from the document top.
                        var sectionBottomPos = id.parent('section').offset().top + id.parent('section').outerHeight() - (hh + 5); //height of the section minus the header height
                        
                        if(sectionPos <= scrollP  && sectionBottomPos > scrollP){
                            $('a[href="' + href + '"]').parent('li').addClass('section-active');
                        } else {
                            $('a[href="' + href + '"]').parent('li').removeClass('section-active');
                        }
                    }
                });
            }
        }
    });
    window.addEventListener('resize', function(){
        hh = $('.header').outerHeight();
    });
}