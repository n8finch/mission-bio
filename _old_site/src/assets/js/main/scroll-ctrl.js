(function(){

    bannerScrollAnimarion();
    function bannerScrollAnimarion(){

        if($('.home-template').length){
           
            var introController;
            var $headerLogo = $('.header-logo');
            var $logoText = $('#logo-text');
            var $bannerContent = $('#banner-content');
            var logoCenterPos;
            var logoAnimation;
            var bannerAnimationBool = false;

            if(window.innerWidth > 1024){
                setBannerAnimation();
                bannerAnimationBool = true;
            }
       
            resizeEvent(function(){
                if(bannerAnimationBool){
                    logoAnimation.pause(0,true);
                    logoAnimation.remove();
                }
            }, function(){
                logoCenterPos = (window.innerWidth * .15);
                if(window.innerWidth > 1024){
                    setBannerAnimation();
                    bannerAnimationBool = true;
                } else if(window.innerWidth <= 1024 && bannerAnimationBool) { 
                    introController.destroy(true);
                    bannerAnimationBool = false;
                }
            }); 
        }
    
        function setBannerAnimation(){
            introController = new ScrollMagic.Controller();
            logoCenterPos = (window.innerWidth * .15);
            logoAnimation = new TimelineMax()
            .fromTo($headerLogo, 1, {x: logoCenterPos},{x: 0})
            .to($logoText, 1, {opacity: 0}, '-=1')
            .to($bannerContent, 0.5, {opacity: 0}, '-=1');
        
            new ScrollMagic.Scene({
                triggerHook: 1,
                duration: '100%',
            })
            .setTween(logoAnimation)
            .addTo(introController);
        }
    }
    

    if($('.round-icon').length){
        iconAnimation();
    }
     
    function iconAnimation(){
        var iconController = new ScrollMagic.Controller();
        var icons = $('.round-icon');

        icons.each(function(){

            new ScrollMagic.Scene({
                triggerElement: this,
                triggerHook: 'onEnter',
                offset: 150,
                reverse: false
            })
            .setClassToggle(this, 'triggered')
            .addTo(iconController);
        });
    
    }
 

})();