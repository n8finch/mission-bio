
function lead_form(cb){
    var emailCheck = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var formTime;
    var submitTime;
    var formBool = true;
    var callback = cb;
    //storing time of first input click;
    $('#lead-form input').on('focus', function(){
        if(formBool){
            formTime = new Date().getTime();
            formBool = false;
        }
    });
    $('#lead-form').on("submit", function(e) {
        e.preventDefault();
        $('.error').remove();
        $('.input-error').removeClass('input-error');
        var errors = {};
        submitTime = new Date().getTime();
        var elapsedTime = submitTime - formTime;
        if (elapsedTime < 1500){
            $('#lead-form').addClass('robo-fill');
            return false;
        }

        var form = $(this);
        var payload = form.serialize();
        var $fullname = $('#lead-form input[name=fullname-1]');
        var $title = $('#lead-form input[name=title-1]');
        var $institutionName = $('#lead-form input[name=institution-1]');
        var $email = $('#lead-form input[name=email-1]');



        // error handling
        if($fullname.val() == ''){
            errors.fullname = "Name is required";
            errors.status = true;
        }
        if($title.val() == ''){
            errors.title = "Title is required";
            errors.status = true;
        }
        if($institutionName.val() == ''){
            errors.institutionName = "Institution Name is required";
            errors.status = true;
        }
        if($email.val() == ''){
            errors.email = "Email is required";
            errors.status = true;
        }
        if (!emailCheck.test($email.val())) {
            errors.email = "Valid email is required";
            errors.status = true;
        }

        if (errors.status) {
            $('.required-prompt').append("<span class='error'>&emsp; – please fill out required fields</span>");
            if(errors.fullname){
                $fullname.addClass('input-error');
                $fullname.parents(".form-item").append("<span class='error'>" + errors.fullname + "</span>");
            }
            if(errors.institutionName){
                $institutionName.addClass('input-error');
                $institutionName.parents(".form-item").append("<span class='error'>" + errors.institutionName + "</span>");
            }
            if(errors.email){
                $email.addClass('input-error');
                $email.parents(".form-item").append("<span class='error'>" + errors.email + "</span>");
            }
            if(errors.title){
                $title.addClass('input-error');
                $title.parents(".form-item").append("<span class='error'>" + errors.title + "</span>");
            }
            return
        }

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '../../php/process-lead.php', // the url where we want to POST
            data        : payload, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode          : true
        })
        // using the done promise callback
        .done(function(data) {
            console.log('message sent');
            return callback();
             
        }).fail(function (data) {
            console.log('message not sent');
        });
    });
}
