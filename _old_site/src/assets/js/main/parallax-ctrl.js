(function(){
    
    if(window.innerWidth > 1024 && $('.parallax-bg').length){
        parallaxCtrl(); 
    }

    function parallaxCtrl(){ 
        var parallaxController = new ScrollMagic.Controller();
        var $parallaxItems = $('.parallax-bg');

        $parallaxItems.each(function(i){
            var parallaxBool = true;
            var parallax = new TimelineMax().from(this, 1, {y: "-20%", ease: Linear.easeNone});
    
            var parallaxScene = new ScrollMagic.Scene({
                triggerElement: this,
                triggerHook: "onEnter",
                duration: "120%"
            })
            .setTween(parallax)
            .addTo(parallaxController);


            resizeEvent(function(){
                //start
            }, function(){
                if(window.innerWidth > 1024 && !parallaxBool){
                    parallaxScene.enabled(true);
                    parallaxBool = true;
                } else if(window.innerWidth <= 1024 && parallaxBool) { 
                    parallaxScene.enabled(false);
                    parallaxBool = false;
                }
            }); 
        });
    }

})();
