(function(){
    if($('.popup-modal').length){
        popupModal();
    }

    function popupModal(){
        var $popupModal = $('.popup-modal');
        var $window = $(window);
        var showPopup = true;
        var data = sessionStorage.getItem('popupBool');
        if(data == "visited"){
            showPopup = false;
        } 

        $window.on('scroll', function(){
            if($window.scrollTop() > $window.height()){
                $popupModal.css({"opacity": 1});
                if(showPopup){
                    $popupModal.trigger("click");
                    showPopup = false; 
                    sessionStorage.setItem('popupBool', 'visited');
                }
            } else {
                $popupModal.css({"opacity": 0});
            }
        });

        $popupModal.on('click touchend', function(e){
            $this = $(this)
            if($popupModal.hasClass('animating')) return;
            if(!$this.hasClass('modal-active')){
                $popupModal.addClass('animating');
                $popupModal.addClass('modal-active')
                setTimeout(function(){
                    $popupModal.addClass('show-modal');
                },50);
                setTimeout(function(){
                    $popupModal.removeClass('animating');
                },400);
            } else if($(e.target).hasClass('modal-active') || $(e.target).hasClass('close-toggle')) {
                $popupModal.addClass('animating');
                $popupModal.removeClass('show-modal')
                setTimeout(function(){
                    $popupModal.removeClass('modal-active');
                    $popupModal.removeClass('animating');
                },400);
            }
            
        });
    }
})();