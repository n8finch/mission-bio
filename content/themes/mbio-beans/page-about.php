<?php

/**
 * Template Name: About Page: Discovery DNA
 */

//* Add landing page body class to the head
add_filter( 'body_class', 'mbio_add_body_class' );
function mbio_add_body_class( $classes ) {
	$classes[] = 'aboutpage aboutpage-template';
	return $classes;
}


beans_remove_markup( 'beans_fixed_wrap_main' );
// beans_remove_attribute( 'beans_main', 'class', 'uk-block' );
// beans_remove_attribute( 'beans_post', 'class', 'uk-panel-box' );

add_action( 'beans_uikit_enqueue_scripts', 'mbio_aboutpage_ui_kit_components_enqueue' );
function mbio_aboutpage_ui_kit_components_enqueue() {

	beans_uikit_enqueue_components( array( 'modal' ) );

}

/**
 * Modify main content area of front page
 * 1. Remove the beans_loop_template
 * 2. Add a class to the beans_content
 * 3. Add custom view content to the beans_content area
 */

beans_remove_action( 'beans_loop_template' );
// beans_add_attribute( 'beans_main', 'id', 'front-page-main-section' );
// beans_add_attribute( 'beans_content', 'class', 'uk-grid uk-grid-collapse' );
beans_add_smart_action( 'beans_content', 'mbio_view_aboutpage_content' );

// add_filter( 'mbio_footer_cta_acf_fields', 'mbio_filter_the_footer_cta', 99, 2 );

// NOTE:
// Header done in content/themes/mbio-beans/template-action-filters/header.php
// Remove and do custom header here with:
// beans_remove_action( 'mbio_call_the_custom_headers' );
// Add Custom Header into the function below.

function mbio_view_aboutpage_content() {
	$acf = get_fields();
	$list_icon =  WP_HOME . '/dist/images/icons/icon_check.svg';
	$play_icon =  WP_HOME . '/dist/images/icons/icon_play.svg';
	$modal_close_icon =  WP_HOME . '/dist/images/ios-close-empty.svg';
	?>

	<section id="about-info-video" style="background-image: url('/dist/images/about-pipette.jpg');">
		<div class="about-container">
			<div class="about-info">
				<?php echo wpautop( $acf['about_info'] ); ?>
				<div class="button-container">
					<?php
						if( $acf['about_cta'] ) {
							mbio_do_cta_buttons( $acf['about_cta'] );
						}
					?>
				</div>
			</div>
			<div class="about-video">
			</div>

		</div>
	</section>
	<section id="about-news-events">
		<div class="about-container">
			<h2><?php echo esc_html( $acf['news_and_events_title'] );?></h2>
			<p><?php echo esc_html( $acf['news_and_events_subtitle'] );?></p>
			<div class="button-container">
				<?php
					if( $acf['newsroom_cta'] ) {
						mbio_do_cta_buttons( $acf['newsroom_cta'] );
					}
				?>
			</div>
		</div>
	</section>
	<section id="about-current-investors">
		<div class="skew-container">
			<div class="about-container">
				<h2>Current Investors</h2>
				<p>Mission Bio is proud to have support from these major investors.</p>
				<div class="investor-logo-ticker">
					<div class="investor-logos-container">
						<div class="custom-navigation">
							<div class="">
								<a href="#" class="flex-prev">
									<img src="/dist/images/left-chevron.svg" alt="next slide">
								</a>
							</div>
							<div class="flexslider carousel">
								<ul class="slides">
									<?php
									if( $acf['investor_logos'] ) {
										foreach( $acf['investor_logos'] as $logo ) {
											?>
											<li>
													<img src="<?php echo esc_url( $logo['logo']['url'] ); ?>" alt="<?php echo esc_url( $logo['logo']['alt'] ); ?>">
											</li>
											<?php
										}
									}
									?>
								</ul>
							</div>
							<div class="">
								<a href="#" class="flex-next">
									<img src="/dist/images/right-chevron.svg" alt="next slide">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="about-career-forward">
		<div class="about-container">
			<h2><?php echo esc_html( $acf['career_title'] );?></h2>
			<div class="button-container">
				<?php
					if( $acf['career_cta'] ) {
						mbio_do_cta_buttons( $acf['career_cta'] );
					}
				?>
			</div>
		</div>
	</section>
	<?php

}

beans_load_document();
