<?php

/**
 * Template Name: Panel Page
 * @var [type]
 */

//* Add landing page body class to the head
add_filter( 'body_class', 'mbio_add_body_class' );
function mbio_add_body_class( $classes ) {
	$classes[] = 'panelpage panelpage-template';
	return $classes;
}

beans_remove_markup( 'beans_fixed_wrap_main' );
// beans_remove_attribute( 'beans_main', 'class', 'uk-block' );
// beans_remove_attribute( 'beans_post', 'class', 'uk-panel-box' );

add_action( 'beans_uikit_enqueue_scripts', 'mbio_panelpage_ui_kit_components_enqueue' );
function mbio_panelpage_ui_kit_components_enqueue() {

	beans_uikit_enqueue_components( array( 'modal' ) );

}

/**
 * Modify main content area of front page
 * 1. Remove the beans_loop_template
 * 2. Add a class to the beans_content
 * 3. Add custom view content to the beans_content area
 */

beans_remove_action( 'beans_loop_template' );
// beans_add_attribute( 'beans_main', 'id', 'front-page-main-section' );
// beans_add_attribute( 'beans_content', 'class', 'uk-grid uk-grid-collapse' );
beans_add_smart_action( 'beans_content', 'mbio_view_panelpage_content' );

// NOTE:
// Header done in content/themes/mbio-beans/template-action-filters/header.php
// Remove and do custom header here with:
// beans_remove_action( 'mbio_call_the_custom_headers' );
// Add Custom Header into the function below.

function mbio_view_panelpage_content() {
	$acf = get_fields();
	$list_icon =  WP_HOME . '/dist/images/icons/icon_check.svg';
	?>

	<section id="panel-hero" style="background-image: url(<?php echo esc_url( $acf['hero_image'] );?>);">
		<div class="hero-title-container">
			<h1><?php echo $acf['hero_title'] ; ?></h1>
		</div>
		<div class="hero-content-container">
			<div class="hero-image">
			</div>
			<div class="hero-content">
				<?php echo $acf['hero_content'] ; ?>
			</div>
		</div>
	</section>

	<section id="panel-content">
		<div class="left-content">
			<h3><?php echo wp_kses_post( $acf['content_left_header'] ); ?></h3>
			<?php echo wp_kses_post( $acf['content_left_content'] ); ?>
		</div>

		<div class="right-content">
			<?php if( $acf['content_right_bullet_list'] ) { ?>
				<div class="red-bullet-list-box">
					<h3><?php echo wp_kses_post( $acf['content_right_list_header'] ); ?></h3>
					<ul class="red-check-bullet-list">
						<?php
							foreach( $acf['content_right_bullet_list'] as $item ) {
								?>
								<li>
									<span><?php echo file_get_contents( $list_icon ); ?></span>
									<span><?php echo wp_kses_post( $item['list_item'] ); ?></span>
								</li>
								<?php
							} ?>
					</ul>
				</div>
			<?php
			}
			?>
		</div>

	</section>

	<?php

}

beans_load_document();
