<?php

/**
 * Template Name: Homepage
 */

//* Add landing page body class to the head
add_filter( 'body_class', 'mbio_add_body_class' );
function mbio_add_body_class( $classes ) {
	$classes[] = 'frontpage frontpage-template';
	return $classes;
}


beans_remove_markup( 'beans_fixed_wrap_main' );
// beans_remove_attribute( 'beans_main', 'class', 'uk-block' );
// beans_remove_attribute( 'beans_post', 'class', 'uk-panel-box' );

add_action( 'beans_uikit_enqueue_scripts', 'mbio_frontpage_ui_kit_components_enqueue' );
function mbio_frontpage_ui_kit_components_enqueue() {

	beans_uikit_enqueue_components( array( 'modal' ) );

}

/**
 * Modify main content area of front page
 * 1. Remove the beans_loop_template
 * 2. Add a class to the beans_content
 * 3. Add custom view content to the beans_content area
 */

beans_remove_action( 'beans_loop_template' );
// beans_add_attribute( 'beans_main', 'id', 'front-page-main-section' );
// beans_add_attribute( 'beans_content', 'class', 'uk-grid uk-grid-collapse' );
beans_add_smart_action( 'beans_content', 'mbio_view_content' );

// NOTE:
// Header done in content/themes/mbio-beans/template-action-filters/header.php
// Remove and do custom header here with:
beans_remove_action( 'mbio_call_the_custom_headers' );
// Add Custom Header into the function below.

function mbio_view_content() {
	$acf = get_fields();
	$list_icon =  WP_HOME . '/dist/images/icons/icon_check.svg';
	$play_icon =  WP_HOME . '/dist/images/icons/icon_play.svg';
	$modal_close_icon =  WP_HOME . '/dist/images/ios-close-empty.svg';

	?>
	<section id="homepage-hero" style="background-image: url('<?php echo esc_url( $acf['hero_background']) ?>')">
		<div class="hero-container">
			<h1><?php echo wp_kses_post( $acf['hero_title'] ); ?></h1>
			<h3><?php echo wp_kses_post( $acf['hero_subtitle'] ); ?></h3>
			<ul class="red-check-bullet-list">
				<?php
				if( $acf['hero_list'] ) {
					foreach( $acf['hero_list'] as $item ) {
						?>
						<li><?php echo file_get_contents( $list_icon ) . ' ' . wp_kses_post( $item['item'] ); ?></li>
						<?php
					}
				}
				?>
			</ul>
			<div class="button-container">
				<?php
					if( isset( $acf['hero_cta'] ) ) {
						mbio_do_cta_buttons( $acf['hero_cta'] );
					}
				?>
			</div>
		</div>

	</section>

	<section id="homepage-applications">
		<h2><?php echo esc_html( $acf['applications_title'] ); ?></h2>
		<div class="application-items">
			<?php
			if( $acf['applications_items'] ) {
				foreach ( $acf['applications_items'] as $item ) {
					?>
					<a href="<?php echo esc_url( $item['link_to_section'] ); ?>">
						<div class="application-item">
							<div class="image-holder">
								<img src="<?php echo esc_url( $item['image'] ); ?>" alt="application">
							</div>
							<p><?php echo esc_html( $item['text'] ); ?></p>
						</div>
					</a>
					<?php
				}
			}
			?>
		</div>
	</section>

	<section id="homepage-missing" style="background-image: url('<?php echo esc_url( $acf['missing_background_image'] ); ?>')">
		<h2><?php echo wp_kses_post( $acf['missing_title']); ?></h2>
		<h4><?php echo wp_kses_post( $acf['missing_subtitle']); ?></h4>
		<p><?php echo wp_kses_post( $acf['missing_content']); ?></p>
		<div class="button-container">
			<a class="button red-button shadow" href="<?php echo esc_url( $acf['missing_button_link'] ); ?>"><?php echo esc_html( $acf['missing_button_text']); ?></a>
		</div>
	</section>

	<section id="homepage-tapestri" style="background-image: url('<?php echo esc_url( $acf['tapestri_background'] ); ?>')">
		<div class="tapestri-container">
			<h2><?php echo esc_html( $acf['tapestri_title'] ); ?></h2>
			<p><?php echo esc_html( $acf['tapestri_subtitle'] ); ?></p>
			<ul class="red-check-bullet-list">
				<?php
				if( $acf['tapestri_list'] ) {
					foreach( $acf['tapestri_list'] as $item ) {
						?>
						<li>
							<span><?php echo file_get_contents( $list_icon ); ?></span>
							<span><?php echo esc_html( $item['item'] ); ?></span>
						</li>
						<?php
					}
				}
				?>
			</ul>
			<div class="button-container">
				<?php
					if( isset( $acf['tapestri_cta'] ) ) {
						mbio_do_cta_buttons( $acf['tapestri_cta'] );
					}
				?>
			</div>
		</div>

	</section>

	<section id="homepage-testimonials">
		<div class="custom-navigation">
			<div class="direction-buttons">
				<a href="#" class="flex-prev">
					<?php echo file_get_contents( WP_HOME . '/dist/images/left-chevron.svg'); ?>
				</a>
			</div>
			<div class="flexslider">
					<ul class="slides">
						<?php
						if( $acf['testimonial_items'] ) {
							foreach( $acf['testimonial_items'] as $item ) {
								?>
								<li>
									<div class="testimonial-item">
										<div class="testimonial-quote-icon-div">
											<?php echo file_get_contents( WP_HOME . '/dist/images/icons/icon_quote.svg'); ?>
										</div>
										<div class="testimonial-quote-div">
											<p><?php echo esc_html( $item['text'] ); ?></p>
											<h4><?php echo esc_html( $item['client'] ); ?></h4>
											<div class="image-holder">
												<img src="<?php echo esc_url( $item['logo'] ); ?>" alt="testimonial logo">
											</div>
										</div>
									</div>
								</li>
								<?php
							}
						}
						?>
					</ul>
			</div>
			<div class="direction-buttons">
				<a href="#" class="flex-next">
					<?php echo file_get_contents( WP_HOME . '/dist/images/right-chevron.svg'); ?>
				</a>
			</div>
		</div>
	</section>

	<section id="homepage-news-events" style="background-image: url('<?php echo esc_url( $acf['news_and_events_background_image'] ); ?>')">
		<h2>News & Events</h2>
		<div class="new-events-items">

			<?php
			//Set up some vars
			$homepage_query = array();
			$homepage_regular_query = array();

			$args = array(
				'post_type'      => array( 'press', 'news', 'events'),
				'category_name'  => 'sticky',
				'order'          => 'DESC',
				'orderby'        => 'date',
				'posts_per_page' => 3,
			);

			$homepage_ticky_query = new WP_Query( $args );
			$sticky_count = count( $homepage_ticky_query->posts );

			$homepage_query = $homepage_ticky_query->posts;

			if( 3 > $sticky_count ) {

				wp_reset_query();

				$args = array(
					'post_type'      => array( 'press', 'news', 'events'),
					'order'          => 'DESC',
					'orderby'        => 'date',
					'posts_per_page' => 3 - $sticky_count,
				);
				$homepage_regular_query = new WP_Query( $args );

				$homepage_query = array_merge( $homepage_query, $homepage_regular_query->posts );
			}

			if($homepage_query) {
				foreach( $homepage_query as $post ) {

					// Replace the events with event so that it is singular
					( 'events' == $post->post_type ) ? $post->post_type = 'event' : '';
					?>
					<a href="<?php echo esc_url( get_the_permalink( $post->ID) ); ?>">
						<div class="equal-heights new-events-item <?php echo esc_html($post->post_type); ?>">
							<div class="item-type"><?php echo ucfirst( esc_html( $post->post_type ) ); ?></div>
							<h4 class="item-title"><?php echo esc_html($post->post_title); ?></h4>
							<!-- <p class="item-info">Just some place holder text for now...</p> -->
							<p class="read-more">Read More <?php echo file_get_contents( WP_HOME . '/dist/images/icons/icon_arrow_right.svg'); ?></p>
						</div>
					</a>
					<?php
				}

			}

			wp_reset_query();
			?>
		</div>
		<div class="button-container">
			<a class="button white-button shadow" href="<?php echo esc_url( get_the_permalink( 37 ) ); ?>">Newsroom</a>
		</div>
	</section>

	<section id="homepage-insights">
		<h2>Insights</h2>
		<div class="insights-items">
			<?php
			if( $acf['insights_items'] ) {
				foreach( $acf['insights_items'] as $item ) {
					?>
					<a href="<?php echo esc_url( $item['link'] ); ?>">
						<div class="insights-item">
							<div class="image-holder">
								<img src="<?php echo esc_url( $item['image'] ); ?>" alt="insight image">
							</div>
							<div class="content-holder">
								<h5 style="color:<?php echo $item['main_color']; ?>"><?php echo esc_html( $item['title'] ); ?></h5>
							</div>
						</div>
					</a>
					<?php
				}
			}
			?>
		</div>
	</section>
	<?php
}

beans_load_document();
