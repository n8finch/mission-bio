<?php

/*--------------------------------------------------------*\
 *	KBD Loader for Custom Functions
 *--------------------------------------------------------*/

add_action( 'wp_head', 'kbd_add_to_head', 1 );
function kbd_add_to_head() {
	?>

	<?php
		// CSS Files
		// =====================================
		// Loads app.css if on local or staging server,
		// and app.min.css if on production.
		//
		// Also, gets critical css if 1) on production server
		// and 2) the critical.min.css file exists
		//

		if ( file_exists( $_SERVER['DOCUMENT_ROOT'] . '/dist/css/critical.min.css' ) ) {
				echo "<style type='text/css'>" . file_get_contents( WP_HOME . "/dist/css/critical.min.css" ) . "</style>";
		}

		$stylesheet = "production" === $GLOBALS[ "ENV" ]
			? "/dist/css/app.min.css"
			: "/dist/css/app.css";

		// Load Files
		// =====================================
		// Uses rel='preload', and supplemented with
		// a <noscript> option.
		//
		// Following script ensures css is loaded
		// if javascript is on, but browser doesn't
		// support rel='preload'
		//
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<link rel="preload" type="text/css" href="<?php echo $stylesheet; ?>" as="style" onload="this.rel='stylesheet'">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">


	<noscript>
		<link rel="stylesheet" href="<?php echo $stylesheet; ?>">
	</noscript>

	<script type="text/javascript">
	var DOMTokenListSupports=function(a,b){if(a&&a.supports)try{return a.supports(b)}catch(a){a instanceof TypeError?console.log("The DOMTokenList doesn't have a supported tokens list"):console.error("That shouldn't have happened")}};window.supportsPreload = DOMTokenListSupports(document.createElement("link").relList, "preload");if (!supportsPreload) {!function(a){"use strict";var b=function(b,c,d){function j(a){return e.body?a():void setTimeout(function(){j(a)})}function l(){f.addEventListener&&f.removeEventListener("load",l),f.media=d||"all"}var g,e=a.document,f=e.createElement("link");if(c)g=c;else{var h=(e.body||e.getElementsByTagName("head")[0]).childNodes;g=h[h.length-1]}var i=e.styleSheets;f.rel="stylesheet",f.href=b,f.media="only x",j(function(){g.parentNode.insertBefore(f,c?g:g.nextSibling)});var k=function(a){for(var b=f.href,c=i.length;c--;)if(i[c].href===b)return a();setTimeout(function(){k(a)})};return f.addEventListener&&f.addEventListener("load",l),f.onloadcssdefined=k,k(l),f};"undefined"!=typeof exports?exports.loadCSS=b:a.loadCSS=b}("undefined"!=typeof global?global:this);!function(a){if(a.loadCSS){var b=loadCSS.relpreload={};if(b.support=function(){try{return a.document.createElement("link").relList.supports("preload")}catch(a){return!1}},b.poly=function(){for(var b=a.document.getElementsByTagName("link"),c=0;c<b.length;c++){var d=b[c];"preload"===d.rel&&"style"===d.getAttribute("as")&&(a.loadCSS(d.href,d),d.rel=null)}},!b.support()){b.poly();var c=a.setInterval(b.poly,300);a.addEventListener&&a.addEventListener("load",function(){a.clearInterval(c)}),a.attachEvent&&a.attachEvent("onload",function(){a.clearInterval(c)})}}}(this);
		loadCSS('<?php echo $stylesheet; ?>')
	}
	</script>


	<script type="text/javascript">
		// add in PressIcon svgs for Press page
		var svgIcons = {
			'stackedLogo': '<?php echo file_get_contents( WP_HOME . '/dist/images/mission-bio-logo.svg') ?>',
			'horizontalLogo': '<?php echo file_get_contents( WP_HOME . '/dist/images/mission-bio-horizontal-logo.svg') ?>',
			'blog' : '<?php echo file_get_contents( WP_HOME . '/dist/images/press-icons/blog.svg') ?>',
			'events' : '<?php echo file_get_contents( WP_HOME . '/dist/images/press-icons/events.svg') ?>',
			'news' : '<?php echo file_get_contents( WP_HOME . '/dist/images/press-icons/news.svg') ?>',
			'press' : '<?php echo file_get_contents( WP_HOME . '/dist/images/press-icons/press.svg') ?>',
		};
	</script>
	<?php
	if( strpos( $_SERVER['HTTP_HOST'] , '.com' ) ) {
		?>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-65254702-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-65254702-1');
		</script>
		<?php
	}
}


add_action( 'wp_footer', 'kbd_add_to_footer', 20);

function kbd_add_to_footer() {
	// JS Files
	// =====================================
	// Loads app.js if on local or staging server,
	// and app.min.js if on production.
	//

	$javascript_file = "production" === $GLOBALS[ "ENV" ]
		? "/dist/javascript/app.min.js"
		: "/dist/javascript/app.js";
	?>

	<script src="https://cdn.polyfill.io/v2/polyfill.min.js" async defer></script>
	<script type="text/javascript" src="<?php echo $javascript_file; ?>" async defer></script>

	<?php
	// Typescript
	// =====================================
	// Loads in Typescript fonts
	//
	?>
	<script>
	  (function(d) {
	    var config = {
	      kitId: 'vej7vkm',
	      scriptTimeout: 3000,
	      async: true
	    },
	    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
	  })(document);
	</script>

	<?php

}
