<?php 
/*--------------------------------------------------------*\
 *	Loader for ACF Config functions
 *--------------------------------------------------------*/

$acf_configs = [
	"acf_search",
	"options_pages",
];

foreach ( $acf_configs as $file ) {
	require_once( "{$file}.php" );
}