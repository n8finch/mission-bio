<?php

$wp_configs = [
	"clean_wp_meta",
	"set_ajax_url",
	"theme_supports",
	"custom_post_types",
	"custom_taxonomies",
	"custom_rest_endpoints",
];

foreach( $wp_configs as $config ) {
	require_once( "{$config}.php" );
}
