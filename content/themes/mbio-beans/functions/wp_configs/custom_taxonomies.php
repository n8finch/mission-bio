<?php
/*--------------------------------------------------------*\
 *	Custom Taxonomies
 *--------------------------------------------------------*
 *
 *	Registers Custom Taxonomies
 *
\*--------------------------------------------------------*/

add_action("init","register_custom_taxonomies");

function register_custom_taxonomies() {

	register_taxonomy(
		'structure',
		'team',
		array(
			'label' => __( 'Structure' ),
			'rewrite' => array( 'slug' => 'structure' ),
			'hierarchical' => true,
		)
	);

	register_taxonomy(
		'resource-type',
		'resources',
		array(
			'label' => __( 'Resource Type' ),
			'rewrite' => array( 'slug' => 'resource-type' ),
			'hierarchical' => true,
		)
	);

}
