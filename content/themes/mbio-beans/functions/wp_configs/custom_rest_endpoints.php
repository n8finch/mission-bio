<?php

/**
 * Grab latest post title by an author!
 *
 * @param array $data Options for the function.
 * @return string|null Post title for the latest,  * or null if none.
 */
function my_awesome_func( $data ) {
	$press_args = array(
		'post_type'      => array( 'press', 'news', 'blog', 'events'),
		'order'          => 'DESC',
		'orderby'        => 'date',
		'posts_per_page' => 30
	);
	$press_query = new WP_Query( $press_args );

	if ( empty( $press_query ) ) {
		return null;
	}

	return $press_query->posts;
}


add_action( 'rest_api_init', function () {
  register_rest_route( 'wp/v2', '/viewallpressitems', array(
    'methods' => 'GET',
    'callback' => 'my_awesome_func',
  ) );
} );
