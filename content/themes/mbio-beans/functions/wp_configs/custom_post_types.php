<?php
/*--------------------------------------------------------*\
 *	Custom Post Types
 *--------------------------------------------------------*
 *
 *	Registers Custom Post Types, e.g. team, blog, etc
 *
\*--------------------------------------------------------*/

add_action("init","register_custom_post_types");

function register_custom_post_types() {

	# Array of Custom Post Types
	// Comment Out Unecessary CPTS
	// Usually will only need Portfolio OR Companies
	$cpts = [
		"blog" => [
			"menu_icon" => "dashicons-media-document",
			"supports" => ['title', 'editor', 'thumbnail', 'author', 'revisions','page-attributes'],
			"show_in_rest" => true,
			'taxonomies'  => array( 'category' ),
		],
		"news" => [
			"menu_icon" => "dashicons-media-document",
			"supports" => ['title', 'editor', 'thumbnail', 'author', 'revisions','page-attributes'],
			"show_in_rest" => true,
			'taxonomies'  => array( 'category' ),
		],
		"press" => [
			"menu_icon" => "dashicons-media-document",
			"supports" => ['title', 'editor', 'thumbnail', 'author', 'revisions','page-attributes'],
			"show_in_rest" => true,
			'taxonomies'  => array( 'category' ),
		],
		"team" =>[
			"menu_icon" => "dashicons-groups",
			"supports" => ['title', 'editor', 'thumbnail', 'author', 'revisions','page-attributes'],
		],
		"events" => [
			"menu_icon" => "dashicons-calendar-alt",
			"show_in_rest" => true,
			'taxonomies'  => array( 'category' ),
		],
		"resources" => [
			"menu_icon" => "dashicons-paperclip",
		],
		// "partners" => [],
		"portfolio" => [
			"menu_icon" => "dashicons-networking",
		],
		// "companies" => ["menu_icon" => "dashicons-networking",],
		"careers" => [
			"menu_icon" => "dashicons-clipboard",
		],
		// "investor" => [],
	];

	# Set Menu Position – 20 = "Below Pages"
	$position = 20;
	foreach ( $cpts as $key => $options ) {

		# Build Labels
		$labels = [
			"name" => _x(ucfirst($key), "post type general name"),
			"singular_name" => _x(ucfirst($key), "post type singular name"),
			"add_new" => __("Add New ", ucfirst($key)),
			"add_new_item" => __("Add New ", ucfirst($key)),
			"edit_item" => __("View ", ucfirst($key)),
			"new_item" => __("New ", ucfirst($key)),
			"view_item" => __("View ", ucfirst($key)),
			"search_items" => __("Search ", ucfirst($key)),
			"not_found" => "Could not find in " . ucfirst($key) . ".",
			"not_found_in_trash" => "Could not find in " . ucfirst($key) . " trash.",
			"parent_item_colon" => "",
		];

		# Set Default Options
		$defaultOptions = [
			"labels" => $labels,
			"public" => true,
			"publicaly_queryable" => true,
			"show_ui" => true,
			"show_in_menu" => true,
			"query_var" => true,
			"rewrite" => true,
			"capability_type" => "post",
			"has_archive" => false,
			"hierarchical" => false,
			"menu_position" => $position,
			"supports" => ["title","editor","thumbnail","revisions","page-attributes"],
			"show_in_nav_menus" => true,
		];

		// Modify default options
		if( 'resources' == $key ||  'news' == $key ) {
			$defaultOptions['public']=false;
			$defaultOptions['publicaly_queryable']=false;
			$defaultOptions['has_archive']=false;
			$defaultOptions['exclude_from_search']=true;
			$defaultOptions['show_ui']=true;
			$defaultOptions['show_in_menu']=true;
		}

		# Merge Options and Register Post
		$args = array_merge($defaultOptions, $options);
		register_post_type($key, $args);
		$position++;
	}
}
