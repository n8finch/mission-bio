<?php 
/*--------------------------------------------------------*\
 *	Clean WP Meta
 *--------------------------------------------------------*
 *
 *	Removes some of the WP generated cruft from <head></head>
 *	
\*--------------------------------------------------------*/
add_action( "init", "clean_wp_meta" );

function clean_wp_meta() {
	remove_action( "wp_head", "rsd_link" );
	remove_action( "wp_head", "wlwmanifest_link" );
	remove_action( "wp_head", "wp_generator" );
	remove_action( "wp_head", "print_emoji_detection_script", 7 );
	remove_action( "admin_print_scripts", "print_emoji_detection_script" );
	remove_action( "admin_print_scripts", "print_emoji_styles" );
	remove_action( "wp_print_styles", "print_emoji_styles" );
}