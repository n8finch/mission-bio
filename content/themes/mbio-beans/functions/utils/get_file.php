<?php 

function get_file( $filename, $resolve_path=false ) {

	if ( "uploads" === $resolve_path ) {
		$path = $_SERVER[ "DOCUMENT_ROOT" ] . explode( $_SERVER[ 'HTTP_HOST' ], $filename)[1];
	}

	elseif (  strpos($filename, ".svg") >  -1 && ! $resolve_path ) {
		$path = $_SERVER[ "DOCUMENT_ROOT" ] . '/dist/images/icons' . $filename;
	}

	else {
		$path = $_SERVER[ "DOCUMENT_ROOT" ] . $filename;
	}

	if ( is_file($path) ) {
		ob_start();
		include $path;
		$file_content = ob_get_contents();
		ob_end_clean();
		return $file_content;
	}

	$error_message = "ERROR: file could not be found at PATH: {$path}";

	return "production" === $GLOBALS["ENV"]
		? "<!-- {$error_message} -->"
		: $error_message;
}