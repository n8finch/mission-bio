<?php

function mbio_do_cta_buttons( $cta_buttons ) {

    if( !$cta_buttons ) {
        return;
    }

    foreach( $cta_buttons as $cta ) {

        $func          = $cta['button_functionality'];
        $color         = $cta['button_color'];
        $shadow        = $cta['shadow'] ? 'shadow' : '';
        $text          = $cta['button_text'];
        $link          = isset( $cta['internal_link'] ) ? $cta['internal_link'] : '';
        $target        = '';
        $data          = '';
        $modal_content = $cta['modal'];
        $modal_close   = file_get_contents( WP_HOME . '/dist/images/ios-close-empty.svg' );
        $hash_class    = '';
        $hash_data     = '';
        $include_play  = isset( $cta['include_play_button'] ) ? $cta['include_play_button'] : '';
        $play_svg      = '';

        if( 'internal' == $func ) {
            if( is_object( $cta['internal_link'] ) ) {
                $link = get_the_permalink( $cta['internal_link']->ID );
            }
        }

        if( 'external' == $func ) {
            $link = $cta['external_link'];
            $target = 'target="_blank"';
        }

        if( 'modal' == $func ) {
            $modal_id = strtolower( str_replace( ' ', '-', $text ) );
            $link = '#' . $modal_id;
            $data = 'data-uk-modal="{center:true}"';

            $modal_content = "<div id=\"{$modal_id}\" class=\"uk-modal\">
			    <div class=\"uk-modal-dialog\">
			        <a class=\"uk-modal-close\">{$modal_close}</a>
			        <div class=\"homepage-modal container\">
                        {$modal_content}
			        </div>
			    </div>
			</div>";
        }

        if( 'hash' == $func ) {
            $hash_class = 'sticky-links';
            $hash_data  = 'data-hash="' . $cta['on_page_hash'] . '"';
        }

        if( 'hash-other' == $func ) {
            $link = $cta['other_page_hash'];
        }

        if( $include_play ) {
            $play_svg = '<span class="play-svg">' . file_get_contents( WP_HOME . '/dist/images/icons/icon_play.svg' ) . '</span>';
        }



        echo "<div><a class=\"button {$color}-button {$shadow} {$hash_class}\" href=\"{$link}\" {$data} {$target} {$hash_data}>{$text} {$play_svg}</a></div>";
        echo $modal_content;

    }


}
