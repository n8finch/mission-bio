<?php

/**
 * Get posts for the Resources page
 * @param  [string] $term [description]
 * @return [array]       [description]
 */
function get_resources_by_taxonomy( string $term ) {
	$args = array(
		'post_type' => 'resources',
		'tax_query' => array(
			array(
				'taxonomy' => 'resource-type',
				'field'    => 'slug',
				'terms'    => $term,
			),
		),
	);
	$query = get_posts( $args );
	wp_reset_query();

	return $query;
}
