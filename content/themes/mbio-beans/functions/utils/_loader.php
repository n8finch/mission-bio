<?php

$utils = [
	"do_cta_buttons",
	"do_blue_arrow_links",
	"flexible-content-page",
	"get_file",
	"get_resources_by_tax",
];

foreach ( $utils as $util ) {
	require_once( "{$util}.php" );
}
