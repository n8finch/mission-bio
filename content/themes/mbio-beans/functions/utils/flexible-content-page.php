<?php

function mbio_do_full_width_content( $group ) {
    ?>
    <div class="full-width-layout">
    <?php

        echo $group['wysiwyg_editor_full'];
        ?>
        <div class="button-container">
            <?php
                if( $group['cta_buttons'] ) {
                    mbio_do_cta_buttons( $group['cta_buttons'] );
                }
            ?>
        </div>
    </div>
    <?php
}

function mbio_do_three_column_layout( $group ) {

    ?>
    <div class="three-column-layout columns">
    <?php

    $counter = 1;

    while( $counter < 4 ) {

        ?>
        <div class="three-column-layout column">
            <?php
            echo $group["wysiwig_{$counter}_of_3"] ;
            ?>
            <div class="button-container">
                <?php
                    if( $group["cta_button_{$counter}_of_3"] ) {
                        mbio_do_cta_buttons( $group["cta_button_{$counter}_of_3"] );
                    }
                ?>
            </div>
        </div>
        <?php
        $counter++;
    }?>

    </div>
    <?php

}
