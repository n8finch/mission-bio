<?php

/**
 * Template Name: Team Page
 * @var [type]
 */

//* Add landing page body class to the head
add_filter( 'body_class', 'mbio_add_body_class' );
function mbio_add_body_class( $classes ) {
	$classes[] = 'teampage teampage-template';
	return $classes;
}

beans_remove_markup( 'beans_fixed_wrap_main' );
// beans_remove_attribute( 'beans_main', 'class', 'uk-block' );
// beans_remove_attribute( 'beans_post', 'class', 'uk-panel-box' );

add_action( 'beans_uikit_enqueue_scripts', 'mbio_teampage_ui_kit_components_enqueue' );
function mbio_teampage_ui_kit_components_enqueue() {

	beans_uikit_enqueue_components( array( 'modal' ) );
}

/**
 * Modify main content area of front page
 * 1. Remove the beans_loop_template
 * 2. Add a class to the beans_content
 * 3. Add custom view content to the beans_content area
 */

beans_remove_action( 'beans_loop_template' );
// beans_add_attribute( 'beans_main', 'id', 'front-page-main-section' );
// beans_add_attribute( 'beans_content', 'class', 'uk-grid uk-grid-collapse' );
beans_add_smart_action( 'beans_content', 'mbio_view_teampage_content' );

add_filter( 'mbio_footer_cta_acf_fields', 'mbio_filter_the_footer_cta', 99, 2 );

// NOTE:
// Header done in content/themes/mbio-beans/template-action-filters/header.php
// Remove and do custom header here with:
// beans_remove_action( 'mbio_call_the_custom_headers' );
// Add Custom Header into the function below.

function mbio_view_teampage_content() {

	global $post;
	$acf = get_fields();

	?>

	<section id=team-people>
		<h2>Management</h2>
		<div id="team-management" class="team-container">

			<?php
			$management_args = array(
				'post_type' => 'team',
				'orderby'   => 'menu_order',
				'order'     => 'ASC',
				'tax_query' => array(
					array(
						'taxonomy' => 'structure',
						'field'    => 'slug',
						'terms'    => 'management',
					),
				),
			);
			$management_query = new WP_Query( $management_args );

			foreach( $management_query->posts as $post ) {

				$acf = get_fields($post->ID);

				?>
				<div class="team-member">
					<a href="#<?php echo $post->post_name; ?>-bio" data-uk-modal="{center:true}">
						<div class="image-container">
							<img src="<?php echo esc_url( $acf['team_member_image']['sizes']['team-member'] ); ?>" alt="<?php echo esc_html( $acf['team_member_name'] ); ?> Photo" />
						</div>
					</a>

					<a href="#<?php echo $post->post_name; ?>-bio" data-uk-modal="{center:true}">
						<h4><?php echo esc_html( $acf['team_member_name'] ); ?></h4>
					</a>

					<p><?php echo esc_html( $acf['team_member_position'] ); ?></p>

					<p><a href="<?php echo esc_html( $acf['team_member_linkedin'] ); ?>" target="_blank"><span class="fab fa-linkedin-in"></span></a></p>

					<div id="<?php echo $post->post_name; ?>-bio" class="uk-modal">
						<div class="uk-modal-dialog">
							<a class="uk-modal-close uk-close"></a>

							<div class="team-member-modal">
								<div class="image-container">
									<img src="<?php echo esc_url( $acf['team_member_image']['sizes']['team-member'] ); ?>" alt="<?php echo esc_html( $acf['team_member_name'] ); ?> Photo" />
								</div>

								<h4><?php echo esc_html( $acf['team_member_name'] ); ?></h4>

								<p><?php echo esc_html( $acf['team_member_position'] ); ?></p>

								<p><a href="<?php echo esc_html( $acf['team_member_linkedin'] ); ?>" target="_blank"><span class="fab fa-linkedin-in"></span></a></p>

								<?php echo wp_kses_post( $acf['team_member_bio'] ); ?>
							</div>
						</div>
					</div>
				</div>
				<?php
			}

			?>


		</div>
		<h2>Board of Directors</h2>
		<div id="team-board" class="team-container">

			<?php
			$management_args = array(
				'post_type' => 'team',
				'orderby'   => 'menu_order',
				'order'     => 'DESC',
				'tax_query' => array(
					array(
						'taxonomy' => 'structure',
						'field'    => 'slug',
						'terms'    => 'board-of-directors',
					),
				),
			);
			$management_query = new WP_Query( $management_args );

			foreach( $management_query->posts as $post ) {

				$acf = get_fields($post->ID);

				?>
				<div class="team-member">
					<a href="#<?php echo $post->post_name; ?>-bio" data-uk-modal="{center:true}">
						<div class="image-container">
							<img src="<?php echo esc_url( $acf['team_member_image']['sizes']['team-member'] ); ?>" alt="<?php echo esc_html( $acf['team_member_name'] ); ?> Photo" />
						</div>
					</a>

					<a href="#<?php echo $post->post_name; ?>-bio" data-uk-modal="{center:true}">
						<h4><?php echo esc_html( $acf['team_member_name'] ); ?></h4>
					</a>

					<p><?php echo esc_html( $acf['team_member_position'] ); ?></p>

					<p><a href="<?php echo esc_html( $acf['team_member_linkedin'] ); ?>" target="_blank"><span class="fab fa-linkedin-in"></span></a></p>

					<div id="<?php echo $post->post_name; ?>-bio" class="uk-modal">
						<div class="uk-modal-dialog">
							<a class="uk-modal-close uk-close"></a>

							<div class="team-member-modal">
								<div class="image-container">
									<img src="<?php echo esc_url( $acf['team_member_image']['sizes']['team-member'] ); ?>" alt="<?php echo esc_html( $acf['team_member_name'] ); ?> Photo" />
								</div>

								<h4><?php echo esc_html( $acf['team_member_name'] ); ?></h4>

								<p><?php echo esc_html( $acf['team_member_position'] ); ?></p>

								<p><a href="<?php echo esc_html( $acf['team_member_linkedin'] ); ?>" target="_blank"><span class="fab fa-linkedin-in"></span></a></p>

								<?php echo wp_kses_post( $acf['team_member_bio'] ); ?>
							</div>
						</div>
					</div>
				</div>
				<?php
			}

			?>
		</div>
	</section>
	<?php
	wp_reset_query();
}

beans_load_document();
