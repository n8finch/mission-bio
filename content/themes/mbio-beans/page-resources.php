<?php

/**
* Template Name: Resources Page
*/

//* Add landing page body class to the head
add_filter( 'body_class', 'mbio_add_body_class' );
function mbio_add_body_class( $classes ) {
    $classes[] = 'resourcespage resourcespage-template';
    return $classes;
}

beans_remove_markup( 'beans_fixed_wrap_main' );
// beans_remove_attribute( 'beans_main', 'class', 'uk-block' );
// beans_remove_attribute( 'beans_post', 'class', 'uk-panel-box' );

add_action( 'beans_uikit_enqueue_scripts', 'mbio_resourcespage_ui_kit_components_enqueue' );
function mbio_resourcespage_ui_kit_components_enqueue() {

    beans_uikit_enqueue_components( array( 'modal' ) );

}

/**
* Modify main content area of front page
* 1. Remove the beans_loop_template
* 2. Add a class to the beans_content
* 3. Add custom view content to the beans_content area
*/

beans_remove_action( 'beans_loop_template' );
// beans_add_attribute( 'beans_main', 'id', 'front-page-main-section' );
// beans_add_attribute( 'beans_content', 'class', 'uk-grid uk-grid-collapse' );
beans_add_smart_action( 'beans_content', 'mbio_view_resourcespage_content' );

add_filter( 'mbio_footer_cta_acf_fields', 'mbio_filter_the_footer_cta', 99, 2 );

// NOTE:
// Header done in content/themes/mbio-beans/template-action-filters/header.php
// Remove and do custom header here with:
// beans_remove_action( 'mbio_call_the_custom_headers' );
// Add Custom Header into the function below.

function mbio_view_resourcespage_content() {
	global $post;

	$acf = get_fields();

	$modal_close_icon =  WP_HOME . '/dist/images/ios-close-empty.svg';
	$accordion_plus =  WP_HOME . '/dist/images/icons/ios-plus.svg';
	$accordion_minus =  WP_HOME . '/dist/images/icons/ios-minus.svg';

    ?>

	<section id="resources-main">

        <?php
		if( $acf['sticky_menu_items'] ) {
		?>
        <div id="resources-sidebar">
            <ul>
			<?php
			foreach( $acf['sticky_menu_items'] as $item ) {
			?>
            <li class="sticky-links" data-hash="<?php echo $item['menu_hash'] ; ?>"><?php echo esc_html( $item['menu_title'] ) ; ?></li>
			<?php } //end foreach?>
            </ul>
        </div>
		<?php } //end if?>

		<div id="resources-content">


			<div id="videos">
				<?php

				$query_video = get_resources_by_taxonomy( 'video' );
				?>

				<h2>Videos</h2>
				<div class="resource-items">
					<?php
					foreach( $query_video as $item ) {
						$i_acf = get_fields($item->ID);

                        // internal, external, modal
                        $button_func = $i_acf['button_functionality'];
                        $button_text = $i_acf['button_text'];
                        $resource_link = '';
                        $target_blank = '';

                        if( $button_func === 'internal' ) {
                            $resource_link = $i_acf['internal_link']->guid;
                        }
                        if( $button_func === 'external' ) {
                            $resource_link = $i_acf['external_link'];
                            $target_blank = ' target="_blank" ';
                        }
                        if( $button_func === 'modal' ) {
                            $resource_link = '#modal-video-' . $item->ID ;
                            $target_blank = ' data-uk-modal="{center:true}" ';
                        }
					?>


					<a href="<?php echo $resource_link; ?>" <?php echo $target_blank; ?>>

                        <!-- The Resource Div -->
    					<div class="resource-item equal-heights">
    						<div class="item-image-holder">
    							<img src="<?php echo esc_url( $i_acf['image'] ); ?>" alt="Mission Bio Resource">
    						</div>
    						<div class="item-content">
    							<h4><?php echo esc_html( $i_acf['title'] ); ?></h4>
    							<p class="item-excerpt"><?php echo esc_html( $i_acf['subtitle'] ); ?></p>
    							<p class="item-cta"><?php echo $button_text . ' ' . file_get_contents( WP_HOME . '/dist/images/resources-arrow.svg'); ?></p>
    						</div>
    					</div>
                        <!-- END The Resource Div -->

					</a>

                    <?php if( $button_func === 'modal' ) { ?>
    					<div id="modal-video-<?php echo $item->ID ; ?>" class="uk-modal">
    						<div class="uk-modal-dialog">
    							<a class="uk-modal-close"><?php echo file_get_contents( $modal_close_icon ); ?></a>
    							<div class="modal container">
                                    <p><?php
                                    // $i_content = apply_filters( 'resource_modal_content', $i_acf['modal'] );
                                    echo do_shortcode( $i_acf['modal']  );
                                    ?></p>
    							</div>
    						</div>
    					</div>
					<?php
                        }//end modal if
					}// end foreach
					?>
				</div> <!-- end resource item -->
			</div>


			<div id="brochures">
				<?php

				$query_brochures = get_resources_by_taxonomy( 'brochures' );
				// d($query_brochures);
				?>

				<h2>Brochures</h2>
                <div class="resource-items">
					<?php
					foreach( $query_brochures as $item ) {
						$i_acf = get_fields($item->ID);

                        // internal, external, modal
                        $button_func = $i_acf['button_functionality'];
                        $button_text = $i_acf['button_text'];
                        $resource_link = '';
                        $target_blank = '';

                        if( $button_func === 'internal' ) {
                            $resource_link = $i_acf['internal_link']->guid;
                        }
                        if( $button_func === 'external' ) {
                            $resource_link = $i_acf['external_link'];
                            $target_blank = ' target="_blank" ';
                        }
                        if( $button_func === 'modal' ) {
                            $resource_link = '#modal-video-' . $item->ID ;
                            $target_blank = ' data-uk-modal="{center:true}" ';
                        }
					?>


					<a href="<?php echo $resource_link; ?>" <?php echo $target_blank; ?>>

                        <!-- The Resource Div -->
    					<div class="resource-item equal-heights">
    						<div class="item-image-holder">
    							<img src="<?php echo esc_url( $i_acf['image'] ); ?>" alt="Mission Bio Resource">
    						</div>
    						<div class="item-content">
    							<h4><?php echo esc_html( $i_acf['title'] ); ?></h4>
    							<p class="item-excerpt"><?php echo esc_html( $i_acf['subtitle'] ); ?></p>
    							<p class="item-cta"><?php echo $button_text . ' ' . file_get_contents( WP_HOME . '/dist/images/resources-arrow.svg'); ?></p>
    						</div>
    					</div>
                        <!-- END The Resource Div -->

					</a>

                    <?php if( $button_func === 'modal' ) { ?>
    					<div id="modal-video-<?php echo $item->ID ; ?>" class="uk-modal">
    						<div class="uk-modal-dialog">
    							<a class="uk-modal-close"><?php echo file_get_contents( $modal_close_icon ); ?></a>
    							<div class="modal container">
                                    <p><?php
                                    // $i_content = apply_filters( 'resource_modal_content', $i_acf['modal'] );
                                    echo do_shortcode( $i_acf['modal']  );
                                    ?></p>
    							</div>
    						</div>
    					</div>
					<?php
                        }//end modal if
					}// end foreach
					?>
				</div> <!-- end resource item -->
			</div>


			<div id="user-guides">
				<?php

				$query_user_guides = get_resources_by_taxonomy( 'user-guides' );
				// d($query_user_guides);
				?>

				<h2>User Guides</h2>
                <div class="resource-items">
					<?php
					foreach( $query_user_guides as $item ) {
						$i_acf = get_fields($item->ID);

                        // internal, external, modal
                        $button_func = $i_acf['button_functionality'];
                        $button_text = $i_acf['button_text'];
                        $resource_link = '';
                        $target_blank = '';

                        if( $button_func === 'internal' ) {
                            $resource_link = $i_acf['internal_link']->guid;
                        }
                        if( $button_func === 'external' ) {
                            $resource_link = $i_acf['external_link'];
                            $target_blank = ' target="_blank" ';
                        }
                        if( $button_func === 'modal' ) {
                            $resource_link = '#modal-video-' . $item->ID ;
                            $target_blank = ' data-uk-modal="{center:true}" ';
                        }
					?>


					<a href="<?php echo $resource_link; ?>" <?php echo $target_blank; ?>>

                        <!-- The Resource Div -->
    					<div class="resource-item equal-heights">
    						<div class="item-image-holder">
    							<img src="<?php echo esc_url( $i_acf['image'] ); ?>" alt="Mission Bio Resource">
    						</div>
    						<div class="item-content">
    							<h4><?php echo esc_html( $i_acf['title'] ); ?></h4>
    							<p class="item-excerpt"><?php echo esc_html( $i_acf['subtitle'] ); ?></p>
    							<p class="item-cta"><?php echo $button_text . ' ' . file_get_contents( WP_HOME . '/dist/images/resources-arrow.svg'); ?></p>
    						</div>
    					</div>
                        <!-- END The Resource Div -->

					</a>

                    <?php if( $button_func === 'modal' ) { ?>
    					<div id="modal-video-<?php echo $item->ID ; ?>" class="uk-modal">
    						<div class="uk-modal-dialog">
    							<a class="uk-modal-close"><?php echo file_get_contents( $modal_close_icon ); ?></a>
    							<div class="modal container">
                                    <p><?php
                                    // $i_content = apply_filters( 'resource_modal_content', $i_acf['modal'] );
                                    echo do_shortcode( $i_acf['modal']  );
                                    ?></p>
    							</div>
    						</div>
    					</div>
					<?php
                        }//end modal if
					}// end foreach
					?>
				</div> <!-- end resource item -->
			</div>



			<div id="data-sets">
				<?php

				$query_data_sets = get_resources_by_taxonomy( 'data-sets' );
				// $query_data_sets = array(1,2,3);
				// d($query_data_sets);
				?>

				<h2>Data Sets</h2>
				<p><?php echo wpautop( wp_kses_post( $acf['data_sets_excerpt'] ) ); ?></p>
				<p>
					<div class="button-container"><a href="<?php echo esc_url( $acf['link_for_softward_download'] ); ?>" class="button red-button shadow"><?php echo esc_html( $acf['button_text'] ); ?></a></div>
				</p>
				<div class="data-set-accordion">
					<ul class="accordion-items">
						<?php
						foreach ($query_data_sets as $item ) {
							$i_acf = get_fields( $item->ID );

                            // internal, external, modal
                            $button_func = $i_acf['button_functionality'];
                            $button_text = $i_acf['button_text'];
                            $resource_link = '';
                            $target_blank = '';

                            if( $button_func === 'internal' ) {
                                $resource_link = $i_acf['internal_link']->guid;
                            }
                            if( $button_func === 'external' ) {
                                $resource_link = $i_acf['external_link'];
                                $target_blank = ' target="_blank" ';
                            }
                            if( $button_func === 'modal' ) {
                                $resource_link = '#modal-video-' . $item->ID ;
                                $target_blank = ' data-uk-modal="{center:true}" ';
                            }

						?>
						<li class="accordion-item">
							<div class="accordion-title">
								<span><?php echo esc_html( $i_acf['set_title'] ); ?></span>
								<div class="accordion-icon-holder">
									<span class="accordion-icon plus"><?php echo file_get_contents( $accordion_plus ); ?></span>
									<span class="accordion-icon minus hidden"><?php echo file_get_contents( $accordion_minus ); ?></span>
								</div>
							</div>
							<div class="accordion-content animated fadeIn hidden">
								<?php echo wpautop( wp_kses_post( $i_acf['set_content'] ) ); ?>
                                <a href="<?php echo $resource_link; ?>" <?php echo $target_blank; ?>>
                                    <p class="item-cta"><?php echo $button_text . ' ' . file_get_contents( WP_HOME . '/dist/images/resources-arrow.svg'); ?></p>
                                </a>
                                <?php if( $button_func === 'modal' ) { ?>
                					<div id="modal-video-<?php echo $item->ID ; ?>" class="uk-modal">
                						<div class="uk-modal-dialog">
                							<a class="uk-modal-close"><?php echo file_get_contents( $modal_close_icon ); ?></a>
                							<div class="modal container">
                                                <p><?php
                                                // $i_content = apply_filters( 'resource_modal_content', $i_acf['modal'] );
                                                echo do_shortcode( $i_acf['modal']  );
                                                ?></p>
                							</div>
                						</div>
                					</div>
            					<?php
                                }//end modal if
                                ?>
							</div>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>


			<div id="publications">
				<?php

				$query_publications_posters = get_resources_by_taxonomy( 'publications-posters' );
				// d($query_publications_posters);
				?>

				<h2>Publications</h2>
                <div class="resource-items">
					<?php
					foreach( $query_publications_posters as $item ) {
						$i_acf = get_fields($item->ID);

                        // internal, external, modal
                        $button_func = $i_acf['button_functionality'];
                        $button_text = $i_acf['button_text'];
                        $resource_link = '';
                        $target_blank = '';

                        if( $button_func === 'internal' ) {
                            if( $i_acf['internal_link'] ) {
                                $resource_link = $i_acf['internal_link']->guid;
                            }
                        }
                        if( $button_func === 'external' ) {
                            $resource_link = $i_acf['external_link'];
                            $target_blank = ' target="_blank" ';
                        }
                        if( $button_func === 'modal' ) {
                            $resource_link = '#modal-video-' . $item->ID ;
                            $target_blank = ' data-uk-modal="{center:true}" ';
                        }
					?>


					<a href="<?php echo $resource_link; ?>" <?php echo $target_blank; ?>>

                        <!-- The Resource Div -->
    					<div class="resource-item equal-heights">
    						<div class="item-image-holder">
    							<img src="<?php echo esc_url( $i_acf['image'] ); ?>" alt="Mission Bio Resource">
    						</div>
    						<div class="item-content">
    							<h4><?php echo esc_html( $i_acf['title'] ); ?></h4>
    							<p class="item-excerpt"><?php echo esc_html( $i_acf['subtitle'] ); ?></p>
    							<p class="item-cta"><?php echo $button_text . ' ' . file_get_contents( WP_HOME . '/dist/images/resources-arrow.svg'); ?></p>
    						</div>
    					</div>
                        <!-- END The Resource Div -->

					</a>

                    <?php if( $button_func === 'modal' ) { ?>
    					<div id="modal-video-<?php echo $item->ID ; ?>" class="uk-modal">
    						<div class="uk-modal-dialog">
    							<a class="uk-modal-close"><?php echo file_get_contents( $modal_close_icon ); ?></a>
    							<div class="modal container">
                                    <p><?php
                                    // $i_content = apply_filters( 'resource_modal_content', $i_acf['modal'] );
                                    echo do_shortcode( $i_acf['modal']  );
                                    ?></p>
    							</div>
    						</div>
    					</div>
					<?php
                        }//end modal if
					}// end foreach
					?>
				</div> <!-- end resource item -->
			</div>


			<div id="scientific-talks">
				<?php

				$query_scientific_talks = get_resources_by_taxonomy( 'scientific-talks' );
				// d($query_scientific_talks);
				?>

				<h2>Scientific Talks</h2>
                <div class="resource-items">
					<?php
					foreach( $query_scientific_talks as $item ) {
						$i_acf = get_fields($item->ID);

                        // internal, external, modal
                        $button_func = $i_acf['button_functionality'];
                        $button_text = $i_acf['button_text'];
                        $resource_link = '';
                        $target_blank = '';

                        if( $button_func === 'internal' ) {
                            $resource_link = $i_acf['internal_link']->guid;
                        }
                        if( $button_func === 'external' ) {
                            $resource_link = $i_acf['external_link'];
                            $target_blank = ' target="_blank" ';
                        }
                        if( $button_func === 'modal' ) {
                            $resource_link = '#modal-video-' . $item->ID ;
                            $target_blank = ' data-uk-modal="{center:true}" ';
                        }
					?>


					<a href="<?php echo $resource_link; ?>" <?php echo $target_blank; ?>>

                        <!-- The Resource Div -->
    					<div class="resource-item equal-heights">
    						<div class="item-image-holder">
    							<img src="<?php echo esc_url( $i_acf['image'] ); ?>" alt="Mission Bio Resource">
    						</div>
    						<div class="item-content">
    							<h4><?php echo esc_html( $i_acf['title'] ); ?></h4>
    							<p class="item-excerpt"><?php echo esc_html( $i_acf['subtitle'] ); ?></p>
    							<p class="item-cta"><?php echo $button_text . ' ' . file_get_contents( WP_HOME . '/dist/images/resources-arrow.svg'); ?></p>
    						</div>
    					</div>
                        <!-- END The Resource Div -->

					</a>

                    <?php if( $button_func === 'modal' ) { ?>
    					<div id="modal-video-<?php echo $item->ID ; ?>" class="uk-modal">
    						<div class="uk-modal-dialog">
    							<a class="uk-modal-close"><?php echo file_get_contents( $modal_close_icon ); ?></a>
    							<div class="modal container">
                                    <p><?php
                                    // $i_content = apply_filters( 'resource_modal_content', $i_acf['modal'] );
                                    echo do_shortcode( $i_acf['modal']  );
                                    ?></p>
    							</div>
    						</div>
    					</div>
					<?php
                        }//end modal if
					}// end foreach
					?>
				</div> <!-- end resource item -->
			</div>


		</div>

	</section>

  	<?php

}

beans_load_document();
