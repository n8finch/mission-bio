<?php

/**
 *  Template Name: Careers Page
 */

//* Add landing page body class to the head
add_filter( 'body_class', 'mbio_add_body_class' );
function mbio_add_body_class( $classes ) {
	$classes[] = 'careerspage careerspage-template';
	return $classes;
}

beans_remove_markup( 'beans_fixed_wrap_main' );
// beans_remove_attribute( 'beans_main', 'class', 'uk-block' );
// beans_remove_attribute( 'beans_post', 'class', 'uk-panel-box' );

// add_action( 'beans_uikit_enqueue_scripts', 'mbio_careerspage_ui_kit_components_enqueue' );
function mbio_careerspage_ui_kit_components_enqueue() {

	// beans_uikit_enqueue_components( array( 'overlay' ) );

}

/**
 * Modify main content area of front page
 * 1. Remove the beans_loop_template
 * 2. Add a class to the beans_content
 * 3. Add custom view content to the beans_content area
 */

beans_remove_action( 'beans_loop_template' );
// beans_add_attribute( 'beans_main', 'id', 'front-page-main-section' );
// beans_add_attribute( 'beans_content', 'class', 'uk-grid uk-grid-collapse' );
beans_add_smart_action( 'beans_content', 'mbio_view_custompage_content' );

add_filter( 'mbio_footer_cta_acf_fields', 'mbio_filter_the_footer_cta', 99, 2 );

// NOTE:
// Header done in content/themes/mbio-beans/template-action-filters/header.php
// Remove and do custom header here with:
// beans_remove_action( 'mbio_call_the_custom_headers' );
// Add Custom Header into the function below.

function mbio_view_custompage_content() {
	global $post;

	$acf = get_fields();

	$the_title = explode( ' ', $acf['title'] );
	$the_title_suffix = array_pop( $the_title );
	$the_title_suffix = ' <span class="title-suffix">'.$the_title_suffix.'</span>';

	$the_title = join( ' ', $the_title ) . $the_title_suffix ;

	?>

	<section id="careers-main">
		<?php
		$careers_args = array(
			'post_type' => 'careers',
		);
		$careers_query = new WP_Query( $careers_args );
		?>
		<div class="careers-job-listings">
			<?php
			$counter = 1;
			foreach ( $careers_query->posts as $post ) {
				?>
				<a href="<?php echo get_post_permalink(); ?>">
					<div class="job-listing listing-<?php echo $counter; ?>">
						<span class="job-listing-title"><?php echo $post->post_title; ?></span>
						<span class="learn-more">Learn More</span>
					</div>
				</a>
				<?php
				$counter++;
			}
			?>

		</div>

		<div class="benefits-container">
			<?php
			if( $acf['benefit_item'] ) {
				foreach( $acf['benefit_item'] as $item ) {
					?>
					<div class="benefit">
						<img src="<?php echo esc_url( $item['icon']['url'] ); ?>" alt="<?php echo esc_html( $item['icon']['alt'] ); ?>">
						<p><?php echo esc_html( $item['description'] ); ?></p>
					</div>
					<?php
				}
			}
			?>
		</div>

	</section>
	<section id="careers-benefits" style="background-image: url('<?php echo esc_url( $acf['benefits_background_image']['url'] ); ?>');">

	</section>
	<?php
	wp_reset_query();
}

beans_load_document();
