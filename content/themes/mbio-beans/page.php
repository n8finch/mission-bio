<?php

//* Add landing page body class to the head
add_filter( 'body_class', 'mbio_add_body_class' );
function mbio_add_body_class( $classes ) {
	$classes[] = 'custompage custompage-template';
	return $classes;
}

beans_remove_markup( 'beans_fixed_wrap_main' );
// beans_remove_attribute( 'beans_main', 'class', 'uk-block' );
// beans_remove_attribute( 'beans_post', 'class', 'uk-panel-box' );

add_action( 'beans_uikit_enqueue_scripts', 'mbio_custompage_ui_kit_components_enqueue' );
function mbio_custompage_ui_kit_components_enqueue() {

	beans_uikit_enqueue_components( array( 'modal' ) );

}

/**
 * Modify main content area of front page
 * 1. Remove the beans_loop_template
 * 2. Add a class to the beans_content
 * 3. Add custom view content to the beans_content area
 */

beans_remove_action( 'beans_loop_template' );
// beans_add_attribute( 'beans_main', 'id', 'front-page-main-section' );
// beans_add_attribute( 'beans_content', 'class', 'uk-grid uk-grid-collapse' );
beans_add_smart_action( 'beans_content', 'mbio_view_custompage_content' );

// NOTE:
// Header done in content/themes/mbio-beans/template-action-filters/header.php
// Remove and do custom header here with:
// beans_remove_action( 'mbio_call_the_custom_headers' );
// Add Custom Header into the function below.

function mbio_view_custompage_content() {
	$acf = get_fields();

	if( isset( $acf['flexible_content'] ) ) {
		?>
		<section id="flexible-content-page">
			<?php
			foreach( $acf['flexible_content'] as $group ) {

				// Call functions in functions/utils/flexible-content-page.php

				if( 'full_width_content' === $group['acf_fc_layout'] ) {
					mbio_do_full_width_content( $group );
				}

				if( 'three_column_layout' === $group['acf_fc_layout'] ) {
					mbio_do_three_column_layout( $group );
				}

			}
			?>

		</section>
		<?php
	}

}

beans_load_document();
