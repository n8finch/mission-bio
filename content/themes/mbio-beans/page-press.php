<?php

/**
 * Template Name: Press Page
 * @var [type]
 */
//* Add landing page body class to the head
add_filter( 'body_class', 'mbio_add_body_class' );
function mbio_add_body_class( $classes ) {
	$classes[] = 'presspage presspage-template';
	return $classes;
}

beans_remove_markup( 'beans_fixed_wrap_main' );
// beans_remove_attribute( 'beans_main', 'class', 'uk-block' );
// beans_remove_attribute( 'beans_post', 'class', 'uk-panel-box' );

// add_action( 'beans_uikit_enqueue_scripts', 'mbio_presspage_ui_kit_components_enqueue' );
function mbio_presspage_ui_kit_components_enqueue() {

	// beans_uikit_enqueue_components( array( 'overlay' ) );

}

/**
 * Modify main content area of front page
 * 1. Remove the beans_loop_template
 * 2. Add a class to the beans_content
 * 3. Add custom view content to the beans_content area
 */

beans_remove_action( 'beans_loop_template' );
// beans_add_attribute( 'beans_main', 'id', 'front-page-main-section' );
// beans_add_attribute( 'beans_content', 'class', 'uk-grid uk-grid-collapse' );
beans_add_smart_action( 'beans_content', 'mbio_view_presspage_content' );

add_filter( 'mbio_footer_cta_acf_fields', 'mbio_filter_the_footer_cta', 99, 2 );

// NOTE:
// Header done in content/themes/mbio-beans/template-action-filters/header.php
// Remove and do custom header here with:
// beans_remove_action( 'mbio_call_the_custom_headers' );
// Add Custom Header into the function below.

function mbio_view_presspage_content() {
	global $post;

	$acf = get_fields();

	?>

	<section id="press-sorter">
		<div class="press-tabs">
			<ul>
				<li class="press-tab" data-filter=".events">Events</li>
				<li class="press-tab" data-filter=".press">Press Releases</li>
				<li class="press-tab" data-filter=".news">News</li>
				<!-- <li class="press-tab" data-filter=".blog">Blog</li> -->
				<li class="press-tab is-checked" data-filter="*">View All</li>
			</ul>
		</div>
		<div class="press-items">

			<?php
			$press_args = array(
				'post_type'      => array( 'press', 'news', 'events'),
				'order'          => 'DESC',
				'orderby'        => 'date',
				'posts_per_page' => 10,
			);
			$press_query = new WP_Query( $press_args );
			$counter = 1;
			foreach ($press_query->posts as $post ) {
				$logo   = WP_HOME . '/dist/images/press-icons/' . $post->post_type .'.svg';
				$type   = ucfirst($post->post_type);
				$link   = get_post_permalink();
				$target = '';
				$acf 	= '';
				$acf    = get_fields($post->ID);

				if( $acf && $acf['external_url'] ) {
					$link = $acf['external_url'];
					$target = 'target="_blank"';
				}
				?>
				<a href="<?php echo esc_url( $link ); ?>" <?php echo esc_html( $target ); ?>>
					<div class="press-item item-<?php echo $counter . ' ' . $post->post_type; ?>">
						<span class="item-icon"><?php echo file_get_contents( $logo ); ?></span>
						<div class="">
							<span class="item-type"><?php echo $type; ?>: </span>
							<span class="item-title"><?php echo $post->post_title; ?></span>
						</div>
						<span class="item-read-more">Read More</span>
					</div>
				</a>
				<?php
				$counter++;
			}
			?>
		</div>

	</section>
	<?php

}

beans_load_document();
