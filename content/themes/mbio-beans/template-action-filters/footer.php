<?php

//* Footer
//*--------------------------------------------------------*/

add_action( 'beans_footer_before_markup', 'mbio_do_footer_cta' );

function mbio_do_footer_cta() {

	$acf = get_fields();

	if( $acf['include_footer_cta_on_this_page'] ) {

		if( 'default' == $acf['use_the_default_cta_footer_or_customize'] ) {
			$acf = get_fields('options' );
		}

		?>
		<div class="pre-footer">
			<div class="pre-footer-text">
				<?php echo esc_html( $acf['footer_cta_text'] ); ?>
			</div>
			<div class="button-container">
				<?php
					if( $acf['footer_cta_button'] ) {
						mbio_do_cta_buttons( $acf['footer_cta_button'] );
					}
				?>
			</div>
		</div>
		<?php
	} else {
		?>
		<style>
		footer.tm-footer {
			border-top: 5px solid #ddd;
		}
		</style>
		<?php
	}


}


add_action( 'widgets_init', 'example_widgets_init' );

function example_widgets_init() {

    register_sidebar( array(
        'name' => __( 'Footer Contact', 'tm-beans' ),
        'id' => 'footer-contact',
        'description' => __( 'This is the contact section of the footer.', 'tm-beans' ),
		'before_widget' => '<div class="footer-logo-contact">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
    ) );

    register_sidebar( array(
        'name' => __( 'Footer Links', 'tm-beans' ),
        'id' => 'footer-links',
        'description' => __( 'This is the links section of the footer', 'tm-beans' ),
        'before_widget' => '<div class="footer-links-container">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
    ) );

}



// Modify footer and add custom footer
beans_modify_action_callback( 'beans_footer_content', 'mbio_filter_footer_content' );

function mbio_filter_footer_content( $html ) {

	if ( is_active_sidebar( 'footer-contact' ) ) {
		dynamic_sidebar( 'footer-contact' );
	}

	if ( is_active_sidebar( 'footer-links' ) ) {
		dynamic_sidebar( 'footer-links' );
	}

}
