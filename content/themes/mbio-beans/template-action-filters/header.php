<?php

//* Header
//*--------------------------------------------------------*/

// Header Logo
add_action( 'beans_site_branding_prepend_markup', 'mbio_modify_header_logo' );

function mbio_modify_header_logo() {
	$site_logo = "/dist/images/missionbio-logo-stacked.svg";
	if ( wp_is_mobile() ) {
		$site_logo = "/dist/images/missionbio-logo-horizontal.svg";
	}

	$html = '<a class="header-site-logo" href="' . home_url() . '" ><img src="' . $site_logo . '"  /></a>';
	echo $html;

}

add_filter( 'walker_nav_menu_start_el', 'mbio_filter_dropdown_menus', 50, 4 );

function mbio_filter_dropdown_menus( $item_output, $item, $depth, $args ) {

	$item_output = str_replace( 'uk-icon-caret-down', 'fa fa-caret-down', $item_output );
	return   $item_output;
}

beans_modify_action_callback( 'beans_primary_menu_offcanvas_button', 'mbio_mobile_menu' );

function mbio_mobile_menu() {

	$mobile_menu = WP_HOME . '/dist/images/navicon.svg';


	if ( ! current_theme_supports( 'offcanvas-menu' ) ) {
		return;
	}

	beans_open_markup_e( 'beans_primary_menu_offcanvas_button', 'a', array(
		'href'              => '#offcanvas_menu',
		'class'             => 'uk-button uk-hidden-large',
		'data-uk-offcanvas' => '',
	) );

		beans_open_markup_e( 'beans_primary_menu_offcanvas_button_icon', 'span', array(
			'class' => 'fa fa-bars uk-margin-small-right',
			'style' => 'max-width: 35px;',
		) );

		// echo file_get_contents( $mobile_menu );

		beans_close_markup_e( 'beans_primary_menu_offcanvas_button_icon', 'span' );

		beans_output_e( 'beans_offcanvas_menu_button', __( '', 'tm-beans' ) );

	beans_close_markup_e( 'beans_primary_menu_offcanvas_button', 'a' );
}




/**
 * CUSTOM HEADERS
 * @var [type]
 */

 //* Add transparent nav to body class
 add_filter( 'body_class', 'mbio_add_transparent_header_class' );

function mbio_add_transparent_header_class( $classes ) {
	$acf = get_fields();

	// Check if it's the homepage/front page or if the custom header is a transparent one
	if( ( $acf['page_header_type'] === 'homepage-trans' ) || ( is_home() || is_front_page() ) ) {
		$classes[] = 'transparent-header-nav';
		return $classes;
	}
	return $classes;
}


beans_add_smart_action( 'beans_content', 'mbio_do_the_custom_headers', 5 );

function mbio_do_the_custom_headers() {
	$acf = get_fields();
	if( $acf['page_header_type'] === 'simple' ) {
		$group = $acf['simple_page_header']
		?>
		<section class="simple-page-header-hero" >
			<div class="hero-content">
				<p><?php echo wp_kses_post( $group['simple_hero_content'] ); ?></p>
				<div class="button-container">
					<?php
						if( isset( $group['simple_hero_cta'] ) ) {
							mbio_do_cta_buttons( $group['simple_hero_cta'] );
						}
					?>
				</div>
			</div>
		</section>
		<?php
	}

	if( 'left-right' === $acf['page_header_type'] ) {
		$group = $acf['left_right'];
		$arrangement = $group['left_right_arrangement'];

		?>
		<section class="left-right-page-header-hero <?php echo $arrangement; ?>" style="background-image: url(<?php echo esc_url( $group['left_right_hero_image'] );?>);">
			<div class="hero-image">
			</div>
			<div class="hero-content">
				<p><?php echo $group['left_right_hero_content'] ; ?></p>
				<div class="button-container">
					<?php
						if( $group['left_right_hero_cta'] ) {
							mbio_do_cta_buttons( $group['left_right_hero_cta'] );
						}
					?>
				</div>
			</div>
		</section>
		<?php
	}

	if( 'homepage-trans' === $acf['page_header_type'] ) {
		$group = $acf['homepage_transparent'];
		$arrangement = $group['left_right_arrangement'];

		?>
		<section class="homepage-transprent-hero <?php echo $arrangement; ?>" style="background-image: url('<?php echo esc_url( $group['left_right_hero_image']) ?>')">
			<div class="hero-container">
				<p><?php echo $group['left_right_hero_content'] ; ?></p>
				<div class="button-container">
					<?php
						if( isset( $group['hero_cta'] ) ) {
							mbio_do_cta_buttons( $group['left_right_hero_cta'] );
						}
					?>
				</div>
			</div>

		</section>
		<?php
	}

}
