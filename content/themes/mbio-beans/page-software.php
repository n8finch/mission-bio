<?php

/**
 * Template Name: Software Page
 * @var [type]
 */

//* Add landing page body class to the head
add_filter( 'body_class', 'mbio_add_body_class' );
function mbio_add_body_class( $classes ) {
	$classes[] = 'softwarepage softwarepage-template';
	return $classes;
}

beans_remove_markup( 'beans_fixed_wrap_main' );
// beans_remove_attribute( 'beans_main', 'class', 'uk-block' );
// beans_remove_attribute( 'beans_post', 'class', 'uk-panel-box' );

add_action( 'beans_uikit_enqueue_scripts', 'mbio_softwarepage_ui_kit_components_enqueue' );
function mbio_softwarepage_ui_kit_components_enqueue() {

	beans_uikit_enqueue_components( array( 'modal' ) );

}

/**
 * Modify main content area of front page
 * 1. Remove the beans_loop_template
 * 2. Add a class to the beans_content
 * 3. Add custom view content to the beans_content area
 */

beans_remove_action( 'beans_loop_template' );
// beans_add_attribute( 'beans_main', 'id', 'front-page-main-section' );
// beans_add_attribute( 'beans_content', 'class', 'uk-grid uk-grid-collapse' );
beans_add_smart_action( 'beans_content', 'mbio_view_softwarepage_content' );

// NOTE:
// Header done in content/themes/mbio-beans/template-action-filters/header.php
// Remove and do custom header here with:
beans_remove_action( 'mbio_call_the_custom_headers' );
// Add Custom Header into the function below.

function mbio_view_softwarepage_content() {
	$acf = get_fields();
	$list_icon =  WP_HOME . '/dist/images/icons/icon_check.svg';
	$modal_close_icon =  WP_HOME . '/dist/images/ios-close-empty.svg';
	$accordion_plus =  WP_HOME . '/dist/images/icons/ios-plus.svg';
	$accordion_minus =  WP_HOME . '/dist/images/icons/ios-minus.svg';
	?>

	<section id="software-hero" style="background-image: url(<?php echo esc_url( $acf['background_image'] );?>); background-position-x: <?php echo esc_html( $acf['image_align'] ); ?>;">
		<div class="hero-left-content">
			<h1><?php echo wp_kses_post( $acf['hero_title'] ); ?></h1>
			<h3><?php echo wp_kses_post( $acf['hero_subtitle'] ); ?></h3>
			<p><?php echo $acf['hero_content']; ?></p>
			<div class="button-container">
				<?php
					if( $acf['hero_cta'] ) {
						mbio_do_cta_buttons( $acf['hero_cta'] );
					}
				?>
			</div>
		</div>
		<div class="hero-right-content">
			<?php if( $acf['hero_list'] ) { ?>
				<div class="red-bullet-list-box">
					<h3><?php echo wp_kses_post( $acf['hero_list_title'] ); ?></h3>
					<ul class="red-check-bullet-list">
						<?php
							foreach( $acf['hero_list'] as $item ) {
								?>
								<li>
									<span><?php echo file_get_contents( $list_icon ); ?></span>
									<span><?php echo esc_html( $item['list_item'] ); ?></span>
								</li>
								<?php
							} ?>
					</ul>
				</div>
			<?php
			}
			?>

		</div>
	</section>

	<section id="how-it-works">
		<h2>How It Works</h2>
		<?php if( $acf['how_it_works_item'] ) { ?>
		<div class="how-it-works-items">
			<?php foreach( $acf['how_it_works_item'] as $item ) { ?>
				<div class="how-it-works-item">
					<img class="style-svg" src="<?php echo esc_url( $item['image'] ); ?>" alt="how it works image">
					<h5><?php echo esc_html( $item['title'] ); ?></h5>
					<p><?php echo esc_html( $item['description'] ); ?></p>
					<ul class="red-check-bullet-list">
						<?php
							foreach( $item['list_items'] as $list_item ) {
								?>
								<li>
									<span><?php echo file_get_contents( $list_icon ); ?></span>
									<span><?php echo esc_html( $list_item['item'] ); ?></span>
								</li>
								<?php
							} ?>
					</ul>
				</div>
			<?php
			}
			?>
		</div>
		<?php } //end if ?>
		<div class="button-container">
			<?php
				if( $acf['how_it_works_cta'] ) {
					mbio_do_cta_buttons( $acf['how_it_works_cta'] );
				}
			?>
		</div>
	</section>

	<section id="download-software" style="background-image: url(<?php echo esc_url( $acf['download_background_image'] );?>);">
		<?php if( $acf['download_item'] ) { ?>
		<div class="download-software-items">

			<?php foreach( $acf['download_item'] as $item ) { ?>
				<div class="download-software-item">
					<h3><?php echo esc_html( $item['title'] ); ?></h3>
					<p><?php echo esc_html( $item['description'] ); ?></p>
					<div class="button-container">
						<?php
							if( $item['download_button'] ) {
								mbio_do_cta_buttons( $item['download_button'] );
							}
						?>
					</div>
					<?php if( $item['list_items'] ) { ?>
					<div class="red-bullet-list-box">
						<ul class="red-check-bullet-list">
							<?php
								foreach( $item['list_items'] as $item ) {
									?>
									<li>
										<span><?php echo file_get_contents( $list_icon ); ?></span>
										<span><?php echo esc_html( $item['item'] ); ?></span>
									</li>
									<?php
								} ?>
						</ul>
					</div>
					<?php }
					?>
				</div>
			<?php
			}
			?>
		</div>
		<?php } //end if ?>
	</section>

	<section id="download-datasets">

		<?php
		$query_data_sets = get_resources_by_taxonomy( 'data-sets' );
		?>

		<h2>Datasets</h2>

		<div class="data-set-accordion">
			<ul class="accordion-items">
				<?php
				foreach ($query_data_sets as $item ) {
					$i_acf = get_fields( $item->ID );

					// internal, external, modal
					$button_func = $i_acf['button_functionality'];
					$button_text = $i_acf['button_text'];
					$resource_link = '';
					$target_blank = '';

					if( $button_func === 'internal' ) {
						$resource_link = $i_acf['internal_link']->guid;
					}
					if( $button_func === 'external' ) {
						$resource_link = $i_acf['external_link'];
						$target_blank = ' target="_blank" ';
					}
					if( $button_func === 'modal' ) {
						$resource_link = '#modal-video-' . $item->ID ;
						$target_blank = ' data-uk-modal="{center:true}" ';
					}
				?>
				<li class="accordion-item">
					<div class="accordion-title">
						<span><?php echo esc_html( $i_acf['set_title'] ); ?></span>
						<div class="accordion-icon-holder">
							<span class="accordion-icon plus"><?php echo file_get_contents( $accordion_plus ); ?></span>
							<span class="accordion-icon minus hidden"><?php echo file_get_contents( $accordion_minus ); ?></span>
						</div>
					</div>
					<div class="accordion-content animated fadeIn hidden">
						<?php echo wpautop( wp_kses_post( $i_acf['set_content'] ) ); ?>
						<a href="<?php echo $resource_link; ?>" <?php echo $target_blank; ?>>
							<p class="item-cta"><?php echo $button_text . ' ' . file_get_contents( WP_HOME . '/dist/images/resources-arrow.svg'); ?></p>
						</a>
						<?php if( $button_func === 'modal' ) { ?>
							<div id="modal-video-<?php echo $item->ID ; ?>" class="uk-modal">
								<div class="uk-modal-dialog">
									<a class="uk-modal-close"><?php echo file_get_contents( $modal_close_icon ); ?></a>
									<div class="modal container">
										<p><?php
										// $i_content = apply_filters( 'resource_modal_content', $i_acf['modal'] );
										echo do_shortcode( $i_acf['modal']  );
										?></p>
									</div>
								</div>
							</div>
						<?php
						}//end modal if
						?>
					</div>
				</li>
				<?php } ?>
			</ul>
		</div>

	</section>

	<?php

}

beans_load_document();
