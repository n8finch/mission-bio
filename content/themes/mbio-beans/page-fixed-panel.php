<?php

/**
 * Template Name: Fixed Panel Page
 *
 */

//* Add landing page body class to the head
add_filter( 'body_class', 'mbio_add_body_class' );
function mbio_add_body_class( $classes ) {
	$classes[] = 'fixedpanelpage fixedpanelpage-template';
	return $classes;
}

beans_remove_markup( 'beans_fixed_wrap_main' );
// beans_remove_attribute( 'beans_main', 'class', 'uk-block' );
// beans_remove_attribute( 'beans_post', 'class', 'uk-panel-box' );

add_action( 'beans_uikit_enqueue_scripts', 'mbio_fixedpanelpage_ui_kit_components_enqueue' );
function mbio_fixedpanelpage_ui_kit_components_enqueue() {

	beans_uikit_enqueue_components( array( 'modal' ) );

}

/**
 * Modify main content area of front page
 * 1. Remove the beans_loop_template
 * 2. Add a class to the beans_content
 * 3. Add custom view content to the beans_content area
 */

beans_remove_action( 'beans_loop_template' );
// beans_add_attribute( 'beans_main', 'id', 'front-page-main-section' );
// beans_add_attribute( 'beans_content', 'class', 'uk-grid uk-grid-collapse' );
beans_add_smart_action( 'beans_content', 'mbio_view_fixedpanelpage_content' );

// NOTE:
// Header done in content/themes/mbio-beans/template-action-filters/header.php
// Remove and do custom header here with:
// beans_remove_action( 'mbio_call_the_custom_headers' );
// Add Custom Header into the function below.

function mbio_view_fixedpanelpage_content() {
	$acf = get_fields();
	$list_icon =  WP_HOME . '/dist/images/icons/icon_check.svg';
	?>

	<section id="main-sticky-section">

		<?php
		if( $acf['sticky_menu_items'] ) {
		?>
		<div id="full-width-sticky-menu">
			<?php
			foreach( $acf['sticky_menu_items'] as $item ) {
			?>
			<div class="applications-sticky-menu-item sticky-links" data-hash="<?php echo $item['menu_hash'] ; ?>"><?php echo esc_html( $item['menu_title'] ) ; ?></div>
			<?php } //end foreach?>
		</div>

		<?php } //end if?>
		<section id="sub-hero-content">
			<?php echo $acf['sub_hero_content'] ; ?>
		</section>
		<section id="hematologic-malignancies">
			<h2><?php echo esc_html( $acf['hm_content_title'] ); ?></h2>
			<div class="fixed-panel-content-container">
				<div class="left-content">
					<h3><?php echo wp_kses_post( $acf['hm_content_left_header'] ); ?></h3>
					<?php echo wp_kses_post( $acf['hm_content_left_content'] ); ?>
					<?php if( $acf['hm_content_left_bullet_list'] ) { ?>
						<div class="red-bullet-list-box">
							<h3><?php echo wp_kses_post( $acf['hm_content_left_list_header'] ); ?></h3>
							<ul class="red-check-bullet-list">
								<?php
									foreach( $acf['hm_content_left_bullet_list'] as $item ) {
										?>
										<li>
											<span><?php echo file_get_contents( $list_icon ); ?></span>
											<span><?php echo wp_kses_post( $item['list_item'] ); ?></span>
										</li>
										<?php
									} ?>
							</ul>
							<div class="button-container">
								<?php
									if( $acf['hm_cta'] ) {
										mbio_do_cta_buttons( $acf['hm_cta'] );
									}
								?>
							</div>
						</div>
					<?php
					}
					?>
				</div>

				<div class="right-content">
					<?php echo wp_kses_post( $acf['hm_content_right_content'] ); ?>
				</div>
			</div>


			<div class="testimonial-item">
				<div class="testimonial-quote-icon-div">
					<?php echo file_get_contents( WP_HOME . '/dist/images/icons/icon_quote.svg'); ?>
				</div>
				<div class="testimonial-quote-div">
					<p><?php echo esc_html( $acf['hm_quote_box']['text'] ); ?></p>
					<h4><?php echo esc_html( $acf['hm_quote_box']['client'] ); ?></h4>
					<div class="image-holder">
						<img src="<?php echo esc_url( $acf['hm_quote_box']['logo'] ); ?>" alt="testimonial logo">
					</div>
				</div>
			</div>



			<div class="fixed-panel-content-container">
				<div class="left-content">
					<h3><?php echo wp_kses_post( $acf['hm_content_left_header_2'] ); ?></h3>
					<?php echo wp_kses_post( $acf['hm_content_left_content_2'] ); ?>
					<?php if( $acf['hm_content_left_bullet_list_2'] ) { ?>
						<div class="red-bullet-list-box">
							<h3><?php echo wp_kses_post( $acf['hm_content_left_list_header_2'] ); ?></h3>
							<ul class="red-check-bullet-list">
								<?php
									foreach( $acf['hm_content_left_bullet_list_2'] as $item ) {
										?>
										<li>
											<span><?php echo file_get_contents( $list_icon ); ?></span>
											<span><?php echo wp_kses_post( $item['list_item'] ); ?></span>
										</li>
										<?php
									} ?>
							</ul>
							<div class="button-container">
								<?php
									if( $acf['hm_cta'] ) {
										mbio_do_cta_buttons( $acf['hm_cta_2'] );
									}
								?>
							</div>
						</div>
					<?php
					}
					?>
				</div>

				<div class="right-content">
					<?php echo wp_kses_post( $acf['hm_content_right_content_2'] ); ?>
				</div>
			</div>
		</section>


		<section id="solid-tumor-profiling" style="background-image: url('<?php echo esc_url( $acf['solid_tumor_profiling_background'] );?>');">
			<h2><?php echo esc_html( $acf['stp_content_title'] ); ?></h2>
			<div class="fixed-panel-content-container">
				<div class="left-content">
					<h3><?php echo wp_kses_post( $acf['stp_content_left_header'] ); ?></h3>
					<?php echo wp_kses_post( $acf['stp_content_left_content'] ); ?>
					<?php if( $acf['stp_content_left_bullet_list'] ) { ?>
						<div class="red-bullet-list-box">
							<h3><?php echo wp_kses_post( $acf['stp_content_left_list_header'] ); ?></h3>
							<ul class="red-check-bullet-list">
								<?php
									foreach( $acf['stp_content_left_bullet_list'] as $item ) {
										?>
										<li>
											<span><?php echo file_get_contents( $list_icon ); ?></span>
											<span><?php echo wp_kses_post( $item['list_item'] ); ?></span>
										</li>
										<?php
									} ?>
							</ul>
							<div class="button-container">
								<?php
									if( $acf['stp_cta'] ) {
										mbio_do_cta_buttons( $acf['stp_cta'] );
									}
								?>
							</div>
						</div>
					<?php
					}
					?>
				</div>

				<div class="right-content">
					<?php echo wp_kses_post( $acf['stp_content_right_content'] ); ?>
				</div>
			</div>
		</section>

		<section id="panel-performance">
			<h2>Panel Performance</h2>
			<div class="fixed-panel-content-container">
				<div class="center-content">
					<?php echo wp_kses_post( $acf['pp_center_content'] ); ?>
				</div>
			</div>
		</section>

	</section>



	<?php

}

beans_load_document();
