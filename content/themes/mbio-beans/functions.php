<?php

// Include Beans. Do not remove the line below.
require_once( get_template_directory() . '/lib/init.php' );

// Include KBD Header Stuff.
require_once( __DIR__ . '/functions/kbd-load-header-footer-assets.php' );

/*--------------------------------------------------------*\
 *	KBD Loader for Custom Functions
 *--------------------------------------------------------*/

$function_directories = [
	"globals",
	"utils",
	"wp_configs",
	"acf_configs",
];

foreach ( $function_directories as $directory ) {
	require_once( __DIR__ . "/functions/{$directory}/_loader.php" );
}

/*--------------------------------------------------------*\
 *	Action and Filters for getting the theme right
 *--------------------------------------------------------*/

// Set the default layout (filter)
beans_add_smart_action( 'beans_default_layout', 'banks_default_layout' );

function banks_default_layout() {

    return 'c';

}

$action_filter_files = [
	"footer",
	"header",
	"main",
];

foreach ( $action_filter_files as $file ) {
	require_once( __DIR__ . "/template-action-filters/{$file}.php" );
}

// Add custom image sizes
add_image_size( 'logo-image', 300, 140, array( 'center', 'center' ) );
add_image_size( 'team-member', 295, 295, array( 'center', 'center' ) );





/**
 * Add the circle before the submenu items
 */
add_action( 'beans_menu_item_link_prepend_markup', 'mbio_subnav_add_icon' );

function mbio_subnav_add_icon() {
	echo '<span class="fa fa-circle"></span>';
}


/**
 * Add HelloBar announcement to JS so they can be read
 */
add_action( 'wp_enqueue_scripts', 'mbio_get_global_options_to_local_script' );

function mbio_get_global_options_to_local_script() {
	$acf_global_settings = get_fields('options');

	wp_localize_script( 'jquery', 'global_scripts',	$acf_global_settings['announcements'] );
}



/**
 * Dequeue Icons, we're using a CDN instead
 */
add_action( 'beans_uikit_enqueue_scripts', 'mbio_dequeue_icons', 15 );

function mbio_dequeue_icons() {

	beans_uikit_dequeue_components( array( 'icon' ) );

}
