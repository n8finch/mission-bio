<?php

/**
 * Template Name: Instrument Page
 * @var [type]
 */

//* Add landing page body class to the head
add_filter( 'body_class', 'mbio_add_body_class' );
function mbio_add_body_class( $classes ) {
	$classes[] = 'instrumentpage instrumentpage-template';
	return $classes;
}

beans_remove_markup( 'beans_fixed_wrap_main' );
// beans_remove_attribute( 'beans_main', 'class', 'uk-block' );
// beans_remove_attribute( 'beans_post', 'class', 'uk-panel-box' );

add_action( 'beans_uikit_enqueue_scripts', 'mbio_instrumentpage_ui_kit_components_enqueue' );
function mbio_instrumentpage_ui_kit_components_enqueue() {

	beans_uikit_enqueue_components( array( 'modal' ) );

}

/**
 * Modify main content area of front page
 * 1. Remove the beans_loop_template
 * 2. Add a class to the beans_content
 * 3. Add custom view content to the beans_content area
 */

beans_remove_action( 'beans_loop_template' );
// beans_add_attribute( 'beans_main', 'id', 'front-page-main-section' );
// beans_add_attribute( 'beans_content', 'class', 'uk-grid uk-grid-collapse' );
beans_add_smart_action( 'beans_content', 'mbio_view_instrumentpage_content' );

// NOTE:
// Header done in content/themes/mbio-beans/template-action-filters/header.php
// Remove and do custom header here with:
beans_remove_action( 'mbio_call_the_custom_headers' );
// Add Custom Header into the function below.

function mbio_view_instrumentpage_content() {
	$acf = get_fields();
	$list_icon =  WP_HOME . '/dist/images/icons/icon_check.svg';
	?>

	<section id="instrument-hero" style="background-image: url(<?php echo esc_url( $acf['background_image'] );?>);">
		<div class="hero-image">
			<img src="<?php echo esc_url( $acf['hero_image'] ); ?>" alt="Tapestri Image" />
		</div>
		<div class="hero-content">
			<h1><?php echo wp_kses_post( $acf['hero_title'] ); ?></h1>
			<p><?php echo esc_html( $acf['hero_subtitle'] ); ?></p>
			<ul class="red-check-bullet-list">
				<?php
				if( $acf['hero_list'] ) {
					foreach( $acf['hero_list'] as $item ) {
						?>
						<li>
							<span><?php echo file_get_contents( $list_icon ); ?></span>
							<span><?php echo esc_html( $item['list_item'] ); ?></span>
						</li>
						<?php
					}
				}
				?>
			</ul>
			<div class="button-container">
				<?php
					if( $acf['cta'] ) {
						mbio_do_cta_buttons( $acf['cta'] );
					}
				?>
			</div>
		</div>
	</section>

	<section id="instrument-content">
		<?php global $post; ?>
		<?php echo wpautop( wp_kses_post( $post->post_content ) ); ?>
	</section>

	<?php

}

beans_load_document();
