<?php

/**
 * Template Name: Platform Solution
 * @var [type]
 */

//* Add landing page body class to the head
add_filter( 'body_class', 'mbio_add_body_class' );
function mbio_add_body_class( $classes ) {
	$classes[] = 'platformsolutionpage platformsolutionpage-template';
	return $classes;
}

beans_remove_markup( 'beans_fixed_wrap_main' );
// beans_remove_attribute( 'beans_main', 'class', 'uk-block' );
// beans_remove_attribute( 'beans_post', 'class', 'uk-panel-box' );

add_action( 'beans_uikit_enqueue_scripts', 'mbio_platform_solution_page_ui_kit_components_enqueue' );
function mbio_platform_solution_page_ui_kit_components_enqueue() {

	beans_uikit_enqueue_components( array( 'modal' ) );

}

/**
 * Modify main content area of front page
 * 1. Remove the beans_loop_template
 * 2. Add a class to the beans_content
 * 3. Add platform_solution_ view content to the beans_content area
 */

beans_remove_action( 'beans_loop_template' );
// beans_add_attribute( 'beans_main', 'id', 'front-page-main-section' );
// beans_add_attribute( 'beans_content', 'class', 'uk-grid uk-grid-collapse' );
beans_add_smart_action( 'beans_content', 'mbio_view_platform_solution_page_content' );

// NOTE:
// Header done in content/themes/mbio-beans/template-action-filters/header.php
// Remove and do custom header here with:
beans_remove_action( 'mbio_call_the_custom_headers' );
// Add Custom Header into the function below.

function mbio_view_platform_solution_page_content() {
	$acf = get_fields();
	$list_icon =  WP_HOME . '/dist/images/icons/icon_check.svg';
	$modal_close_icon =  WP_HOME . '/dist/images/ios-close-empty.svg';
	$accordion_plus =  WP_HOME . '/dist/images/icons/ios-plus.svg';
	$accordion_minus =  WP_HOME . '/dist/images/icons/ios-minus.svg';
	$quote_item = $acf['subclonal_right_content'];

	?>

	<section id="platform-solution-hero" style="background-image: url(<?php echo esc_url( $acf['background_image'] );?>); background-position-x: <?php echo esc_html( $acf['image_align'] ); ?>;">
		<div class="hero-left-content flex-align-left">
			<h1><?php echo wp_kses_post( $acf['hero_title'] ); ?></h1>
			<h2><?php echo wp_kses_post( $acf['hero_subtitle'] ); ?></h2>
			<p><?php echo $acf['hero_content']; ?></p>
			<div class="button-container">
				<?php
					if( isset( $acf['hero_cta'] ) ) {
						mbio_do_cta_buttons( $acf['hero_cta'] );
					}
				?>
			</div>
		</div>
		<div class="hero-right-content flex-align-right">
			<div class="hero-right-image-background" style="background-image: url(''); ">
				<a class="" href="#platform-solution-video" data-uk-modal="{center:true}">
					<div class="play-button-container">
						<img src="<?php echo esc_url( $acf['hero_right_image'] ); ?>" alt="play video">
					</div>
				</a>
			</div>
			<div id="platform-solution-video" class="uk-modal">
				<div class="uk-modal-dialog">
 				   <a class="uk-modal-close"><?php echo file_get_contents( $modal_close_icon ); ?></a>
 				   <div class="about-dialog-video container">
 					   <?php echo $acf['hero_right_modal_content']; ?>
 				   </div>
 			   </div>
			</div>
		</div>
	</section>

	<section id="evolution-subclonal">
		<div class="flex-align-left">
			<p><?php echo $acf['subclonal_left_content'] ; ?></p>
		</div>
		<div class="flex-align-right">
			<div class="testimonial-item">
				<div class="testimonial-quote-icon-div">
					<?php echo file_get_contents( WP_HOME . '/dist/images/icons/icon_quote.svg'); ?>
				</div>
				<div class="testimonial-quote-div">
					<p><?php echo esc_html( $quote_item['text'] ); ?></p>
					<h4><?php echo esc_html( $quote_item['client'] ); ?></h4>
					<div class="image-holder">
						<img src="<?php echo esc_url( $quote_item['logo'] ); ?>" alt="testimonial logo">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="break-out">
		<div class="flex-align-left">
			<p><?php echo $acf['break_out_left_content'] ; ?></p>
		</div>
		<div class="flex-align-right">
			<img class="style-svg" src="<?php echo esc_url( $acf['break_out_right_image'] ); ?>" alt="break out image">
		</div>
	</section>

	<section id="meet-tapestri" style="background-image: url('<?php echo esc_url( $acf['meet_tapestri_image'] ); ?>');">
		<div class="flex-align-left">
			<!-- <img class="style-svg" src="" alt="break out image"> -->
		</div>
		<div class="flex-align-right">
			<p><?php echo wp_kses_post( $acf['meet_tapestri_content'] )?></p>
		</div>
	</section>

	<section id="how-tapestri-works">
		<h2>How the Tapestri Platform Works</h2>

		<?php if( $acf['explanation_items'] ) {
			$counter = 1;
			$can_hover = '';
			?>
			<div class="tapestri-items">

				<?php foreach( $acf['explanation_items'] as $item ) {
					$can_hover = '';
					if( '' !== $item['content'] ) { $can_hover = 'can-hover';}
					?>
					<?php if( 1 < $counter ) {
						?>
						<div class="tapestri-item-divider">
							<?php echo file_get_contents( WP_HOME . '/dist/images/tapestri-item-divider.svg'); ?>
						</div>
						<?php
					} // end if?>

					<div class="tapestri-item <?php echo $can_hover; ?>" data-description="tapestri-description-<?php echo $counter; ?>">
						<img class="style-svg" src="<?php echo esc_url( $item['image'] )?>" alt="tapestri explanation image">
					</div>
				<?php
				$counter++;
			} //end foreach
				?>
			</div>
		<?php } //end if?>

		<?php if( $acf['explanation_items'] ) {
			$counter = 1;
			?>
		<div class="tapestri-items-descriptions">
			<?php foreach( $acf['explanation_items'] as $item ) { ?>
			<div class="tapestri-items-description animated fadeIn hidden" id="tapestri-description-<?php echo $counter; ?>">
				<?php echo wp_kses_post( $item['content'] ); ?>
			</div>
			<?php
			$counter++;
			} //end foreach ?>
		</div>
		<?php } //end if?>

		<div class="button-container">
			<?php
				if( isset( $acf['how_it_works_cta'] ) ) {
					mbio_do_cta_buttons( $acf['how_it_works_cta'] );
				}
			?>
		</div>
	</section>


	<?php
	// Make some quick vars
	$row_1 = $acf['high_sensitivity_row_1'];
	$row_2 = $acf['high_sensitivity_row_2'];
	?>
	<section id="high-sensitivity">
		<div class="high-sensitivity-container">
			<div class="high-sensitivity-content">
				<p><?php echo wp_kses_post( $row_1['content'] ); ?></p>
				<?php if( $row_1['list_items'] ) { ?>
					<div class="red-bullet-list-box">
						<h3><?php echo wp_kses_post( $row_1['list_title'] ); ?></h3>
						<ul class="red-check-bullet-list">
							<?php
								foreach( $row_1['list_items'] as $item ) {
									?>
									<li>
										<span><?php echo file_get_contents( $list_icon ); ?></span>
										<span><?php echo esc_html( $item['item'] ); ?></span>
									</li>
									<?php
								} ?>
						</ul>
					</div>
				<?php
				}
				?>
			</div>
			<div class="high-sensitivity-images">
				<img class="style-svg" src="<?php echo esc_url( $row_1['chart'] ); ?>" alt="graphs and charts">
			</div>
		</div>
		<div class="high-sensitivity-container">
			<div class="high-sensitivity-images">
				<img class="style-svg" src="<?php echo esc_url( $row_2['chart'] ); ?>" alt="graphs and charts">
			</div>
			<?php if( $row_2['list_items'] ) { ?>
				<div class="red-bullet-list-box">
					<h3><?php echo wp_kses_post( $row_2['list_title'] ); ?></h3>
					<ul class="red-check-bullet-list">
						<?php
							foreach( $row_2['list_items'] as $item ) {
								?>
								<li>
									<span><?php echo file_get_contents( $list_icon ); ?></span>
									<span><?php echo esc_html( $item['item'] ); ?></span>
								</li>
								<?php
							} ?>
					</ul>
				</div>
			<?php
			}
			?>
		</div>
	</section>

	<?php
}

beans_load_document();
