<?php

/**
 * Template Name: Applications Page
 * @var [type]
 */

//* Add landing page body class to the head
add_filter( 'body_class', 'mbio_add_body_class' );
function mbio_add_body_class( $classes ) {
	$classes[] = 'applicationspage applicationspage-template';
	return $classes;
}

beans_remove_markup( 'beans_fixed_wrap_main' );
// beans_remove_attribute( 'beans_main', 'class', 'uk-block' );
// beans_remove_attribute( 'beans_post', 'class', 'uk-application-box' );

add_action( 'beans_uikit_enqueue_scripts', 'mbio_applicationspage_ui_kit_components_enqueue' );
function mbio_applicationspage_ui_kit_components_enqueue() {

	beans_uikit_enqueue_components( array( 'modal' ) );

}

/**
 * Modify main content area of front page
 * 1. Remove the beans_loop_template
 * 2. Add a class to the beans_content
 * 3. Add custom view content to the beans_content area
 */

beans_remove_action( 'beans_loop_template' );
// beans_add_attribute( 'beans_main', 'id', 'front-page-main-section' );
// beans_add_attribute( 'beans_content', 'class', 'uk-grid uk-grid-collapse' );
beans_add_smart_action( 'beans_content', 'mbio_view_applicationspage_content' );

// NOTE:
// Header done in content/themes/mbio-beans/template-action-filters/header.php
// Remove and do custom header here with:
// beans_remove_action( 'mbio_call_the_custom_headers' );
// Add Custom Header into the function below.

function mbio_view_applicationspage_content() {
	$acf = get_fields();
	$list_icon =  WP_HOME . '/dist/images/icons/icon_check.svg';
	?>

	<section id="main-sticky-section">
		<?php
		if( $acf['sticky_menu_items'] ) {
		?>
		<div id="full-width-sticky-menu">
			<?php
			foreach( $acf['sticky_menu_items'] as $item ) {
			?>
			<div class="applications-sticky-menu-item sticky-links" data-hash="<?php echo $item['menu_hash'] ; ?>"><?php echo esc_html( $item['menu_title'] ) ; ?></div>
			<?php } //end foreach?>
		</div>

		<?php } //end if?>

		<section id="hematologic-malignancies" style="background-image: url(<?php echo esc_url( $acf['hm_background_image'] );?>);">
			<div class="left-content">
				<h3><?php echo wp_kses_post( $acf['hm_content_left_header'] ); ?></h3>
				<?php echo wp_kses_post( $acf['hm_content_left_content'] ); ?>
			</div>

			<div class="right-content">
				<?php if( $acf['hm_content_right_link_list'] ) { ?>
					<div class="blue-arrow-link-box">
						<h3><?php echo wp_kses_post( $acf['hm_content_right_list_header'] ); ?></h3>
						<ul class="blue-arrow-link-list">
							<?php
								if( $acf['hm_content_right_link_list'] ) {
									mbio_do_blue_arrow_links( $acf['hm_content_right_link_list'] );
								}
							?>
						</ul>
					</div>
				<?php
				}
				?>
			</div>
		</section>

		<section id="solid-tumor-profiling" style="background-image: url('<?php echo esc_url( $acf['stp_background_image'] );?>');">
			<div class="left-content">
				<h3><?php echo wp_kses_post( $acf['stp_content_left_header'] ); ?></h3>
				<?php echo wp_kses_post( $acf['stp_content_left_content'] ); ?>
			</div>

			<div class="right-content">
				<?php if( $acf['stp_content_right_link_list'] ) { ?>
					<div class="blue-arrow-link-box">
						<h3><?php echo wp_kses_post( $acf['stp_content_right_list_header'] ); ?></h3>
						<ul class="blue-arrow-link-list">
							<?php
								if( $acf['stp_content_right_link_list'] ) {
									mbio_do_blue_arrow_links( $acf['stp_content_right_link_list'] );
								}
							?>
						</ul>
					</div>
				<?php
				}
				?>
			</div>
		</section>

		<section id="genome-editing" style="background-image: url('<?php echo esc_url( $acf['ge_background_image'] );?>');">
			<div class="left-content">
				<h3><?php echo wp_kses_post( $acf['ge_content_left_header'] ); ?></h3>
				<?php echo wp_kses_post( $acf['ge_content_left_content'] ); ?>
			</div>

			<div class="right-content">
				<?php if( $acf['ge_content_right_link_list'] ) { ?>
					<div class="blue-arrow-link-box">
						<h3><?php echo wp_kses_post( $acf['ge_content_right_list_header'] ); ?></h3>
						<ul class="blue-arrow-link-list">
							<?php
								if( $acf['ge_content_right_link_list'] ) {
									mbio_do_blue_arrow_links( $acf['ge_content_right_link_list'] );
								}
							?>
						</ul>
					</div>
				<?php
				}
				?>
			</div>
		</section>

	</section>



	<?php

	// mbio_filter_the_footer_cta( $acf );

}

beans_load_document();
