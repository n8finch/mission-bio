<?php
/*--------------------------------------------------------*\
 *	Loader for Custom Functions
 *--------------------------------------------------------*/

$function_directories = [
	"globals",
	"utils",
	"wp_configs",
	"acf_configs",
];

foreach ( $function_directories as $directory ) {
	require_once( "functions/{$directory}/_loader.php" );
}
