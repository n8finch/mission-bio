<!doctype html>
<html lang="en-us">

<head>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title></title>

	<?php 
		// CSS Files
		// =====================================
		// Loads app.css if on local or staging server,
		// and app.min.css if on production.
		// 
		// Also, gets critical css if 1) on production server
		// and 2) the critical.min.css file exists
		// 
		
		if ( "production" === $GLOBALS[ "ENV" ] && file_exists("/dist/css/critical.min.css") ) {
				echo "<style type='text/css'>" . get_file( "/dist/css/critical.min.css" ) . "</style>";
		}
		
		$stylesheet = "production" === $GLOBALS[ "ENV" ]
			? "/dist/css/app.min.css"
			: "/dist/css/app.css";

		// Load Files
		// =====================================
		// Uses rel='preload', and supplemented with 
		// a <noscript> option.
		// 
		// Following script ensures css is loaded
		// if javascript is on, but browser doesn't
		// support rel='preload'
		//
	?>

	<link rel="preload" type="text/css" href="<?php echo $stylesheet; ?>" as="style" onload="this.rel='stylesheet'">

	<noscript>
		<link rel="stylesheet" href="<?php echo $stylesheet; ?>">
	</noscript>
	
	<script type="text/javascript">
	var DOMTokenListSupports=function(a,b){if(a&&a.supports)try{return a.supports(b)}catch(a){a instanceof TypeError?console.log("The DOMTokenList doesn't have a supported tokens list"):console.error("That shouldn't have happened")}};window.supportsPreload = DOMTokenListSupports(document.createElement("link").relList, "preload");if (!supportsPreload) {!function(a){"use strict";var b=function(b,c,d){function j(a){return e.body?a():void setTimeout(function(){j(a)})}function l(){f.addEventListener&&f.removeEventListener("load",l),f.media=d||"all"}var g,e=a.document,f=e.createElement("link");if(c)g=c;else{var h=(e.body||e.getElementsByTagName("head")[0]).childNodes;g=h[h.length-1]}var i=e.styleSheets;f.rel="stylesheet",f.href=b,f.media="only x",j(function(){g.parentNode.insertBefore(f,c?g:g.nextSibling)});var k=function(a){for(var b=f.href,c=i.length;c--;)if(i[c].href===b)return a();setTimeout(function(){k(a)})};return f.addEventListener&&f.addEventListener("load",l),f.onloadcssdefined=k,k(l),f};"undefined"!=typeof exports?exports.loadCSS=b:a.loadCSS=b}("undefined"!=typeof global?global:this);!function(a){if(a.loadCSS){var b=loadCSS.relpreload={};if(b.support=function(){try{return a.document.createElement("link").relList.supports("preload")}catch(a){return!1}},b.poly=function(){for(var b=a.document.getElementsByTagName("link"),c=0;c<b.length;c++){var d=b[c];"preload"===d.rel&&"style"===d.getAttribute("as")&&(a.loadCSS(d.href,d),d.rel=null)}},!b.support()){b.poly();var c=a.setInterval(b.poly,300);a.addEventListener&&a.addEventListener("load",function(){a.clearInterval(c)}),a.attachEvent&&a.attachEvent("onload",function(){a.clearInterval(c)})}}}(this);
		loadCSS('<?php echo $stylesheet; ?>')
	}
	</script>

	<?php
		//	Call wp_head() and close out
		//	<head></head> section
		// =====================================
		wp_head();
	?>
</head>

<body>