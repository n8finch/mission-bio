<?php 
/*--------------------------------------------------------*\
	Global Variables
 *--------------------------------------------------------*
 *
 *	Use this file to declare global variables. The loader
 *	function found in ../functions.php loads this file
 *	first, giving access to these globals from all files.
 * 
\*--------------------------------------------------------*/

$GLOBALS[ "ENV" ] = "development";