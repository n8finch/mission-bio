<?php 
/*--------------------------------------------------------*\
 *	Add Options Page for Global Site Options
 *--------------------------------------------------------*
 *
 * 
 * 
\*--------------------------------------------------------*/

if ( function_exists('acf_add_options_page') ) {

	# Global Theme Settings Parent Page
	$parent = acf_add_options_page( array(
		'page_title' => 'Global Settings',
		'menu_title' => 'Global Settings',
		'menu_slug'  => 'global-theme-settings',
		'capability' => 'edit_posts',
		'redirect'   => false
	) );

	# Setting Subpage Example
	# Uncomment and edit to include subpage
	// acf_add_options_sub_page( array(
	// 	'page_title'  => 'Subpage Settings',
	// 	'menu_title'  => 'Subpage Settings',
	// 	'parent_slug' => $parent[ 'menu_slug' ],
	// 	'redirect'    => false,
	// ) );
}