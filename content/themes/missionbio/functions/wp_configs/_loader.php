<?php 

$wp_configs = [
	"clean_wp_meta",
	"set_ajax_url",
	"theme_supports",
	"custom_post_types",
];

foreach( $wp_configs as $config ) {
	require_once( "{$config}.php" );
}