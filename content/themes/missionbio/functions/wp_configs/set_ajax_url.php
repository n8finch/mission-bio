<?php 
/*--------------------------------------------------------*\
 *	Set AJAX URL
 *--------------------------------------------------------*
 *
 *	Prints AJAX URL in <head></head> as global variable
 *	
\*--------------------------------------------------------*/
add_action( "wp_head", "set_ajax_url" );

function set_ajax_url() { 
?>
	<script>var ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";</script>
<?php }