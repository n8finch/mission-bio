<?php 
/*--------------------------------------------------------*\
 *	Theme Supports
 *--------------------------------------------------------*
 *
 *	Adds support for title-tag, post-thumbnails, and html5
 *	search.
 *	
\*--------------------------------------------------------*/
if (function_exists("add_theme_support")) {
	add_theme_support( "title-tag" );
	
	add_theme_support( "post-thumbnails" );
	
	add_theme_support( "html5", array( "search-form" ));
	$thumbnailSizes = [
		"2400",
		"1800",
		"1440",
		"1280",
		"1024",
		"768",
		"480",
		"240",
	];
	foreach( $thumbnailSizes as $size ) {
		add_image_size( $size, intval($size), 9999 );
	}
}
