<?php
// JS Files
// =====================================
// Loads app.js if on local or staging server,
// and app.min.js if on production.
//

$javascript_file = "production" === $GLOBALS[ "ENV" ]
	? "/dist/javascript/app.min.js"
	: "/dist/javascript/app.js";
?>

<script src="https://cdn.polyfill.io/v2/polyfill.min.js" async defer></script>
<script type="text/javascript" src="<?php echo $javascript_file; ?>" async defer></script>

<?php
	//	Call wp_footer() and close out
	//	<body> section and <html>
	// =====================================
	wp_footer();
?>
</body>
</html>